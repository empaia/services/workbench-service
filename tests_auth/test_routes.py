import json

import requests

from .settings import Settings

settings = Settings()
wbs_url = settings.wbs_url.strip("/")


EXCLUDED_PATHS = []


def _test_routes(api_version):
    r = requests.get(f"{wbs_url}/{api_version}/openapi.json")
    openapi = json.loads(r.content)
    for path, ops in openapi["paths"].items():
        if path in EXCLUDED_PATHS:
            continue

        path_no_vars = path.replace("{", "").replace("}", "")
        url = f"{wbs_url}/{api_version}{path_no_vars}"
        for op in ops.keys():
            print(op, url)
            r = requests.request(op, url)
            assert r.status_code == 403


def test_routes_v2():
    _test_routes(api_version="v2")


def test_routes_v2_scopes():
    _test_routes(api_version="v2/scopes")


def test_routes_v3():
    _test_routes(api_version="v3")


def test_routes_v3_scopes():
    _test_routes(api_version="v3/scopes")
