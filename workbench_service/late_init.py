class AlreadyInitializedError(Exception):
    pass


class LateInit:
    def __init__(self):
        self._daemon_tick = None

    @property
    def daemon_tick(self):
        return self._daemon_tick

    @daemon_tick.setter
    def daemon_tick(self, daemon_tick):
        if self._daemon_tick is not None:
            raise AlreadyInitializedError("Daemon Tick already initialized.")

        self._daemon_tick = daemon_tick
