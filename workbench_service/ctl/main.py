import logging
import sys

import typer

# Import api.v1, because there would be an unresolved circular import otherwise when importing api.v2:
# from ..api import v1 as _  # pylint: disable=unused-import
from ..models.utils.access_token_tools import AccessTokenTools
from ..settings import Settings

logger = logging.getLogger("wbsctl")
logger.addHandler(logging.StreamHandler(sys.stdout))

app = typer.Typer()


@app.command()
def create_access_token_tool_keys():
    settings = Settings()
    keys_creator = AccessTokenTools(settings.rsa_keys_directory)
    keys_creator.create_rsa_key_files(logger)


@app.command()
def unused_command():
    pass


def main():
    app()
