from typing import List, Literal

from pydantic import Field

from workbench_service.models.marketplace.app import ApiVersion

from ....models.v3.commons import Id, ItemCount, RestrictedBaseModel, Timestamp
from ....models.v3.examination import ExaminationCreatorType, ExaminationState, PreprocessingTrigger, Scope, ScopeToken
from ....models.v3.job import Job
from .apps import App


class PostExamination(RestrictedBaseModel):
    case_id: str = Field(example="b10648a7-340d-43fc-a2d9-4d91cc86f33f", description="Case ID")
    app_id: str = Field(example="b10648a7-340d-43fc-a2d9-4d91cc86f33f", description="App ID")


class Examination(PostExamination):
    id: Id = Field(example="b10648a7-340d-43fc-a2d9-4d91cc86f33f", description="ID")
    creator_id: Id = Field(example="b10648a7-340d-43fc-a2d9-4d91cc86f33f", description="Creator ID")
    creator_type: ExaminationCreatorType = Field(example=ExaminationCreatorType.USER, description="Creator Type")
    state: ExaminationState = Field(example=ExaminationState.OPEN, description="Examination state")
    created_at: Timestamp = Field(
        example=1598611645000, description="Timestamp (milliseconds) when the examination was created"
    )
    updated_at: Timestamp = Field(
        example=1598611645000, description="Timestamp (milliseconds) when the examination was last updated"
    )
    jobs: List[Job] = Field(description="List of job IDs in examination")
    jobs_count: ItemCount = Field(example=10, description="Number of jobs (including finished) in examination")
    jobs_count_finished: ItemCount = Field(example=8, description="Number of finished jobs in examination")
    api_version: Literal[ApiVersion.V2, ApiVersion.V3] = Field(
        example=ApiVersion.V3, description="Defines the API compatibility"
    )
    app: App = Field(description="App of this examination")


class ExaminationList(RestrictedBaseModel):
    items: List[Examination] = Field(description="List of examinations (affected by skip/limit pagination) in case")
    item_count: ItemCount = Field(
        example=10, description="Number of examinations (not affected by skip/limit pagination) in case"
    )


class ScopeTokenAndScopeID(ScopeToken):
    scope_id: Id = Field(
        example="a10648a7-340d-43fc-a2d9-4d91cc86f33f",
        description="The ID of the scope corresponding to the token",
    )


class ExtendedScope(Scope):
    case_id: Id = Field(example="b10648a7-340d-43fc-a2d9-4d91cc86f33f", description="Case ID")
    examination_state: ExaminationState = Field(
        example=ExaminationState.OPEN, description="State of the scopes examination"
    )
    ead: dict = Field(description="EMPAIA app description")


class PostPreprocessingTrigger(RestrictedBaseModel):
    portal_app_id: Id = Field(example="b10648a7-340d-43fc-a2d9-4d91cc86f33f", description="App ID")
    tissue: str = Field(example="LUNG", description="Tissue Type")
    stain: str = Field(example="HE", description="Stain Type")


class ExtendedPreprocessingTrigger(PreprocessingTrigger):
    app: App = Field(description="Corresponding app")


class ExtendedPreprocessingTriggerList(RestrictedBaseModel):
    item_count: ItemCount = Field(example=123, description="Count of items")
    items: List[ExtendedPreprocessingTrigger]
