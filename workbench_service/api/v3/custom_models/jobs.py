from typing import List, Optional

from pydantic import Field, conlist

from workbench_service.models.v3.commons import Id, RestrictedBaseModel
from workbench_service.models.v3.job import JobMode
from workbench_service.models.v3.job import PostJob as MdsPostJob

JOB_FINISHED_STATES = [
    "COMPLETED",
    "FAILED",
    "TIMEOUT",
    "ERROR",
    "INCOMPLETE",
]

JOB_UNFINISHED_STATES = ["READY", "SCHEDULED", "RUNNING"]

JOB_STATES_UNKNOWN_JOBS_BY_JES = ["NONE", "ASSEMBLY"]


class JobScopeQuery(RestrictedBaseModel):
    creators: Optional[List[Id]] = Field(
        example=[
            "b10648a7-340d-43fc-a2d9-4d91cc86f33f",
            "b10648a7-340d-43fc-a2d9-4d91cc86f33f",
        ],
        description="Creator IDs",
    )
    modes: Optional[conlist(JobMode, min_length=1)] = Field(
        example=["PREPROCESSING", "POSTPROCESSING"], description="List of job modes"
    )


class PostJob(MdsPostJob):
    pass


del PostJob.model_fields["app_id"]

PostJob.update_forward_refs()
