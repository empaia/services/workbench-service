from typing import Dict, List, Literal, Optional

from pydantic import AnyHttpUrl, Field

from ....models.v3.commons import Id, ItemCount, RestrictedBaseModel, Timestamp

ImageResponses = {200: {"content": {"image/*": {}}}}


class TagMapping(RestrictedBaseModel):
    tag: str
    mappings: Dict[str, str]


class Slide(RestrictedBaseModel):
    id: Id = Field(description="ID")
    mds_url: Optional[AnyHttpUrl] = Field(
        example="https://mds.example.org",
        description="Base URL of Medical Data Service instance that generated empaia_id",
    )
    local_id: Optional[str] = Field(
        example="3a1e3715-8945-4c13-8fc6-9c1c549cf85d", description="Local ID provided by AP-LIS"
    )
    type: Literal["slide"] = Field(default="slide", example="slide", description="Slide")
    tissue: Optional[TagMapping] = Field(
        example={
            "tag": "SKIN",
            "mappings": {"EN": "Skin", "DE": "Haut"},
        },
        description="Tissue Mapping",
    )
    stain: Optional[TagMapping] = Field(
        example={
            "tag": "H_AND_E",
            "mappings": {"EN": "H&E", "DE": "H&E"},
        },
        description="Stain Mapping",
    )
    block: Optional[str] = Field(example="A1", description="Block")
    case_id: Id = Field(description="Case ID")
    created_at: Timestamp = Field(
        example=1598611645000, description="Timestamp (milliseconds) when the slide was created"
    )
    updated_at: Timestamp = Field(
        example=1598611645000, description="Timestamp (milliseconds) when the slide was last updated"
    )
    deleted: Optional[bool] = Field(
        example=False, description="Flag indicating whether the underlying slide files and mappings have been deleted"
    )


class JobSlideList(RestrictedBaseModel):
    items: List[Slide] = Field(description="List of slides (affected by skip/limit pagination)")
    item_count: ItemCount = Field(example=20, description="Number of slides (not affected by skip/limit pagination)")


class SlideList(RestrictedBaseModel):
    items: List[Slide] = Field(description="List of slides (affected by skip/limit pagination)")
    item_count: ItemCount = Field(example=20, description="Number of slides (not affected by skip/limit pagination)")
