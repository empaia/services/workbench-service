from fastapi import Header

from ..connectors import es_connector
from ..custom_models.examinations import (
    ExtendedPreprocessingTrigger,
    ExtendedPreprocessingTriggerList,
    PostPreprocessingTrigger,
)
from ..singletons import api_integration


def add_routes(app, late_init):
    @app.post(
        "/configuration/preprocessing-triggers",
        tags=["configuration"],
        status_code=200,
        responses={200: {"model": ExtendedPreprocessingTrigger}},
    )
    async def _(
        preprocessing_trigger: PostPreprocessingTrigger,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        return await es_connector.post_preprocessing_trigger(
            preprocessing_trigger=preprocessing_trigger, user_id=user_id
        )

    @app.get(
        "/configuration/preprocessing-triggers",
        tags=["configuration"],
        status_code=200,
        responses={200: {"model": ExtendedPreprocessingTriggerList}},
    )
    async def _(
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        return await es_connector.get_extended_preprocessing_triggers()

    @app.get(
        "/configuration/preprocessing-triggers/{preprocessing_trigger_id}",
        tags=["configuration"],
        status_code=200,
        responses={200: {"model": ExtendedPreprocessingTrigger}},
    )
    async def _(
        preprocessing_trigger_id: str,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        return await es_connector.get_extended_preprocessing_trigger(preprocessing_trigger_id=preprocessing_trigger_id)

    @app.delete(
        "/configuration/preprocessing-triggers/{preprocessing_trigger_id}",
        tags=["configuration"],
        status_code=200,
        responses={200: {"model": ExtendedPreprocessingTrigger}},
    )
    async def _(
        preprocessing_trigger_id: str,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        return await es_connector.delete_preprocessing_trigger(preprocessing_trigger_id=preprocessing_trigger_id)
