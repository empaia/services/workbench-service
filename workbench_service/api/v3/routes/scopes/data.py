from typing import Annotated, Dict

from fastapi import Header, Path, Request
from fastapi.responses import StreamingResponse

from workbench_service.api.v3.connectors import es_connector

from .....models.v3.annotation.annotations import (
    Annotation,
    AnnotationCountResponse,
    AnnotationList,
    AnnotationQuery,
    AnnotationQueryPosition,
    Annotations,
    AnnotationUniqueClassesQuery,
    AnnotationViewerList,
    AnnotationViewerQuery,
    PostAnnotations,
    UniqueClassValues,
)
from .....models.v3.annotation.classes import Class, Classes, ClassList, ClassQuery, PostClasses
from .....models.v3.annotation.collections import (
    Collection,
    CollectionList,
    CollectionQuery,
    ItemPostResponse,
    ItemQuery,
    ItemQueryList,
    PostCollection,
    PostItems,
)
from .....models.v3.annotation.pixelmaps import (
    Pixelmap,
    PixelmapList,
    PixelmapQuery,
    Pixelmaps,
    PixelmapSlideInfo,
    PostPixelmaps,
)
from .....models.v3.annotation.primitives import PostPrimitives, Primitive, PrimitiveList, PrimitiveQuery, Primitives
from .....models.v3.commons import Id, IdObject, Message, UniqueReferences
from ... import params
from ...connectors import annot_connector
from ...custom_models.annotations import ClassesDict
from ...singletons import api_integration, http_client, scope_validation, settings

PixelmapTileResponse = {
    200: {
        "content": {
            "application/octet-stream": {"schema": {"type": "string", "format": "binary"}},
        },
        "description": "Pixelmap tile response.",
    },
    404: {"model": Message, "description": "Pixelmap not found"},
}


def add_routes(app, late_init):
    # ANNOTATIONS

    @app.put(
        "/{scope_id}/annotations/query",
        tags=["data"],
        summary="Query annotations",
        status_code=200,
        responses={200: {"model": AnnotationList}},
    )
    async def _(
        query: AnnotationQuery,
        with_classes: bool = False,
        with_low_npp_centroids: bool = False,
        skip: int = None,
        limit: int = None,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await annot_connector.validate_query(scope_id, query, "annotations")
        return await annot_connector.fetch_annotations(
            annotation_query=query,
            with_classes=with_classes,
            with_low_npp_centroids=with_low_npp_centroids,
            skip=skip,
            limit=limit,
            case_id=await es_connector.get_case_id(scope_id),
        )

    @app.put(
        "/{scope_id}/annotations/query/count",
        tags=["data"],
        summary="Get count of queried annotation items",
        status_code=200,
        responses={200: {"model": AnnotationCountResponse}},
    )
    async def _(
        query: AnnotationQuery,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await annot_connector.validate_query(scope_id, query, "annotations")
        return await annot_connector.fetch_annotations_route_suffix(
            query.model_dump(), "count", case_id=await es_connector.get_case_id(scope_id)
        )

    @app.put(
        "/{scope_id}/annotations/query/unique-class-values",
        tags=["data"],
        summary="Get unique class values of queried annotations",
        status_code=200,
        responses={200: {"model": UniqueClassValues}},
    )
    async def _(
        query: AnnotationUniqueClassesQuery,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await annot_connector.validate_query(scope_id, query, "annotations")
        return await annot_connector.fetch_annotations_route_suffix(
            query.model_dump(), "unique-class-values", case_id=await es_connector.get_case_id(scope_id)
        )

    @app.put(
        "/{scope_id}/annotations/query/unique-references",
        tags=["data"],
        summary="Get unique references of queried annotations",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(
        query: AnnotationQuery,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await annot_connector.validate_query(scope_id, query, "annotations")
        return await annot_connector.fetch_annotations_route_suffix(
            query.model_dump(), "unique-references", case_id=await es_connector.get_case_id(scope_id)
        )

    @app.put(
        "/{scope_id}/annotations/query/viewer",
        tags=["data"],
        summary="Query annotations for viewing purposes",
        status_code=200,
        responses={200: {"model": AnnotationViewerList}},
    )
    async def _(
        query: AnnotationViewerQuery,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await annot_connector.validate_query(scope_id, query, "annotations")
        return await annot_connector.fetch_annotations_route_suffix(
            query.model_dump(), "viewer", case_id=await es_connector.get_case_id(scope_id)
        )

    @app.put(
        "/{scope_id}/annotations/{annotation_id}/query",
        tags=["data"],
        summary="Get the postion of an annotation in the result query",
        status_code=200,
        responses={200: {"model": AnnotationQueryPosition}},
    )
    async def _(
        query: AnnotationQuery,
        annotation_id: str = params.annotation_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await annot_connector.validate_query(scope_id, query, "annotations")
        return await annot_connector.fetch_annotation_query_position(
            query, annotation_id, case_id=await es_connector.get_case_id(scope_id)
        )

    @app.post(
        "/{scope_id}/annotations",
        tags=["data"],
        summary="Post annotations",
        status_code=201,
        responses={201: {"model": Annotations}},
    )
    async def _(
        annotations: PostAnnotations,
        is_roi: bool = False,
        external_ids: bool = False,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        await annot_connector.validate_post_data(scope_id, annotations)
        raw_annotation = await annot_connector.post_annotations(
            annotations, external_ids, case_id=await es_connector.get_case_id(scope_id)
        )
        if is_roi:
            await annot_connector.post_roi_class(
                scope_id, raw_annotation["id"], case_id=await es_connector.get_case_id(scope_id)
            )
        return await annot_connector.fetch_raw_annotation(
            raw_annotation["id"], with_classes=True, case_id=await es_connector.get_case_id(scope_id)
        )

    @app.get(
        "/{scope_id}/annotations/{annotation_id}",
        tags=["data"],
        summary="Get annotation by ID",
        status_code=200,
        responses={200: {"model": Annotation}},
    )
    async def _(
        annotation_id: str = params.annotation_id,
        with_classes: bool = False,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await annot_connector.fetch_raw_annotation(
            annotation_id, with_classes, case_id=await es_connector.get_case_id(scope_id)
        )

    @app.delete(
        "/{scope_id}/annotations/{annotation_id}",
        tags=["data"],
        summary="Delete annotation by ID",
        status_code=200,
        responses={200: {"model": IdObject}},
    )
    async def _(
        annotation_id: str = params.annotation_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        return await annot_connector.delete_annotation(
            scope_id, annotation_id, case_id=await es_connector.get_case_id(scope_id)
        )

    # CLASSES

    @app.put(
        "/{scope_id}/classes/query",
        tags=["data"],
        summary="Query classes",
        status_code=200,
        responses={200: {"model": ClassList}},
    )
    async def _(
        query: ClassQuery,
        with_unique_class_values: bool = False,
        skip: int = None,
        limit: int = None,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await annot_connector.validate_query(scope_id, query, "classes")
        return await annot_connector.fetch_classes(
            class_query=query,
            with_unique_class_values=with_unique_class_values,
            skip=skip,
            limit=limit,
            case_id=await es_connector.get_case_id(scope_id),
        )

    @app.put(
        "/{scope_id}/classes/query/unique-references",
        tags=["data"],
        summary="Get unique references of queried classes",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(
        query: ClassQuery,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await annot_connector.validate_query(scope_id, query, "classes")
        return await annot_connector.fetch_classes_unique_references(
            query, case_id=await es_connector.get_case_id(scope_id)
        )

    @app.post(
        "/{scope_id}/classes",
        tags=["data"],
        summary="Post classes",
        status_code=201,
        responses={201: {"model": Classes}},
    )
    async def _(
        classes: PostClasses,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        await annot_connector.validate_post_data(scope_id, classes)
        await annot_connector.validate_class_namespace(scope_id, classes)
        return await annot_connector.post_classes(classes, case_id=await es_connector.get_case_id(scope_id))

    @app.get(
        "/{scope_id}/classes/{class_id}",
        tags=["data"],
        summary="Get class by ID",
        status_code=200,
        responses={200: {"model": Class}},
    )
    async def _(
        class_id: str = params.class_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await annot_connector.fetch_raw_class(class_id, case_id=await es_connector.get_case_id(scope_id))

    @app.get(
        "/{scope_id}/class-namespaces",
        tags=["data"],
        summary="Get class namespaces",
        status_code=200,
        responses={200: {"model": Dict[str, ClassesDict]}},
    )
    async def _(
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await annot_connector.fetch_classes_namespaces(scope_id)

    @app.delete(
        "/{scope_id}/classes/{class_id}",
        tags=["data"],
        summary="Delete class by ID",
        status_code=200,
        responses={200: {"model": IdObject}},
    )
    async def _(
        class_id: str = params.class_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        return await annot_connector.delete_class(scope_id, class_id, case_id=await es_connector.get_case_id(scope_id))

    # COLLECTIONS

    @app.put(
        "/{scope_id}/collections/query",
        tags=["Deprecated"],
        summary="Query collections",
        status_code=200,
        responses={200: {"model": CollectionList}},
    )
    async def _(
        query: CollectionQuery,
        skip: int = None,
        limit: int = None,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        """
        **ENDPOINT IS DEPRECATED**
        """
        await api_integration.scope_hook(scope_id, payload)
        await annot_connector.validate_query(scope_id, query, "collections")
        return await annot_connector.fetch_collections(
            collection_query=query, skip=skip, limit=limit, case_id=await es_connector.get_case_id(scope_id)
        )

    @app.put(
        "/{scope_id}/collections/query/unique-references",
        tags=["data"],
        summary="Get unique references of queried collections",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(
        query: CollectionQuery,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await annot_connector.validate_query(scope_id, query, "collections")
        return await annot_connector.fetch_collections_unique_references(
            query, case_id=await es_connector.get_case_id(scope_id)
        )

    @app.put(
        "/{scope_id}/collections/{collection_id}/items/query",
        tags=["data"],
        summary="Query items of collection",
        status_code=200,
        responses={200: {"model": ItemQueryList}},
    )
    async def _(
        query: ItemQuery,
        collection_id: str = params.collection_id,
        skip: int = None,
        limit: int = None,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await annot_connector.fetch_item_query_list(
            item_query=query,
            collection_id=collection_id,
            skip=skip,
            limit=limit,
            case_id=await es_connector.get_case_id(scope_id),
        )

    @app.put(
        "/{scope_id}/collections/{collection_id}/items/query/unique-references",
        tags=["data"],
        summary="Get unique references of queried items of a collection",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(
        query: ItemQuery,
        collection_id: str = params.collection_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await annot_connector.fetch_item_query_list_unique_reference(
            item_query=query, collection_id=collection_id, case_id=await es_connector.get_case_id(scope_id)
        )

    @app.post(
        "/{scope_id}/collections",
        tags=["data"],
        summary="Post collection",
        status_code=201,
        responses={201: {"model": Collection}},
    )
    async def _(
        collection: PostCollection,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        await annot_connector.validate_post_data_recursive(scope_id, collection)
        return await annot_connector.post_collection(collection, case_id=await es_connector.get_case_id(scope_id))

    @app.get(
        "/{scope_id}/collections/{collection_id}",
        tags=["data"],
        summary="Get collection by ID",
        status_code=200,
        responses={200: {"model": Collection}},
    )
    async def _(
        collection_id: str = params.collection_id,
        shallow: bool = False,
        with_leaf_ids: bool = False,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await annot_connector.fetch_raw_collection(
            collection_id,
            shallow=shallow,
            with_leaf_ids=with_leaf_ids,
            case_id=await es_connector.get_case_id(scope_id),
        )

    @app.delete(
        "/{scope_id}/collections/{collection_id}",
        tags=["data"],
        summary="Delete collection by ID",
        status_code=200,
        responses={200: {"model": IdObject}},
    )
    async def _(
        collection_id: str = params.collection_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        return await annot_connector.delete_collection(
            scope_id, collection_id, case_id=await es_connector.get_case_id(scope_id)
        )

    @app.post(
        "/{scope_id}/collections/{collection_id}/items",
        tags=["data"],
        summary="Add new items to existing collection",
        status_code=201,
        responses={200: {"model": ItemPostResponse}},
    )
    async def _(
        items: PostItems,
        collection_id: str = params.collection_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        await annot_connector.validate_post_items(scope_id, items)
        return await annot_connector.post_items(collection_id, items, case_id=await es_connector.get_case_id(scope_id))

    @app.delete(
        "/{scope_id}/collections/{collection_id}/items/{item_id}",
        tags=["data"],
        summary="Delete collection item from existing collection",
        status_code=200,
        responses={200: {"model": IdObject}},
    )
    async def _(
        collection_id: str = params.collection_id,
        item_id: str = params.item_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        return await annot_connector.delete_item(
            scope_id, collection_id, item_id, case_id=await es_connector.get_case_id(scope_id)
        )

    # PRIMITIVES

    @app.put(
        "/{scope_id}/primitives/query",
        tags=["data"],
        summary="Query primitives",
        status_code=200,
        responses={200: {"model": PrimitiveList}},
    )
    async def _(
        query: PrimitiveQuery,
        skip: int = None,
        limit: int = None,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await annot_connector.validate_query(scope_id, query, "primitives")
        return await annot_connector.fetch_primitives(
            primitive_query=query, skip=skip, limit=limit, case_id=await es_connector.get_case_id(scope_id)
        )

    @app.put(
        "/{scope_id}/primitives/query/unique-references",
        tags=["data"],
        summary="Get unique references of queried primitives",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(
        query: PrimitiveQuery,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await annot_connector.validate_query(scope_id, query, "primitives")
        return await annot_connector.fetch_primitives_unique_references(
            query, case_id=await es_connector.get_case_id(scope_id)
        )

    @app.post(
        "/{scope_id}/primitives",
        tags=["data"],
        summary="Post primitives",
        status_code=201,
        responses={201: {"model": Primitives}},
    )
    async def _(
        primitives: PostPrimitives,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        await annot_connector.validate_post_data(scope_id, primitives)
        return await annot_connector.post_primitives(primitives, case_id=await es_connector.get_case_id(scope_id))

    @app.get(
        "/{scope_id}/primitives/{primitive_id}",
        tags=["data"],
        summary="Get primitive by ID",
        status_code=200,
        responses={200: {"model": Primitive}},
    )
    async def _(
        primitive_id: str = params.primitive_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await annot_connector.fetch_raw_primitive(primitive_id, case_id=await es_connector.get_case_id(scope_id))

    @app.delete(
        "/{scope_id}/primitives/{primitive_id}",
        tags=["data"],
        summary="Delete primitive by ID",
        status_code=200,
        responses={200: {"model": IdObject}},
    )
    async def _(
        primitive_id: str = params.primitive_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        return await annot_connector.delete_primitive(
            scope_id, primitive_id, case_id=await es_connector.get_case_id(scope_id)
        )

    # PIXELMAPS

    @app.post(
        "/{scope_id}/pixelmaps",
        tags=["data"],
        summary="Post pixelmaps",
        status_code=201,
        responses={201: {"model": Pixelmaps}},
    )
    async def _(pixelmaps: PostPixelmaps, scope_id: Id = Path(...), payload=scope_validation.scope_depends()):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        await annot_connector.validate_post_data(scope_id, pixelmaps)
        return await annot_connector.post_pixelmaps(pixelmaps, case_id=await es_connector.get_case_id(scope_id))

    @app.put(
        "/{scope_id}/pixelmaps/query",
        tags=["data"],
        summary="Query pixelmaps",
        status_code=200,
        responses={200: {"model": PixelmapList}},
    )
    async def _(
        query: PixelmapQuery,
        skip: int = None,
        limit: int = None,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await annot_connector.validate_query(scope_id, query, "pixelmaps")
        return await annot_connector.fetch_pixelmaps(
            pixelmap_query=query, skip=skip, limit=limit, case_id=await es_connector.get_case_id(scope_id)
        )

    @app.put(
        "/{scope_id}/pixelmaps/query/unique-references",
        tags=["data"],
        summary="Get unique references of queried pixelmaps",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(
        query: PixelmapQuery,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await annot_connector.validate_query(scope_id, query, "pixelmaps")
        return await annot_connector.fetch_pixelmaps_unique_references(
            query, case_id=await es_connector.get_case_id(scope_id)
        )

    @app.get(
        "/{scope_id}/pixelmaps/{pixelmap_id}",
        tags=["data"],
        summary="Get pixelmap by ID",
        status_code=200,
        responses={200: {"model": Pixelmap}, 404: {"model": Message, "description": "Pixelmap not found"}},
    )
    async def _(
        pixelmap_id: str = params.pixelmap_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await annot_connector.fetch_raw_pixelmap(pixelmap_id, case_id=await es_connector.get_case_id(scope_id))

    @app.delete(
        "/{scope_id}/pixelmaps/{pixelmap_id}",
        tags=["data"],
        summary="Delete pixelmap by ID",
        status_code=200,
        responses={
            200: {"model": IdObject},
            404: {"model": Message, "description": "Pixelmap not found"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        pixelmap_id: str = params.pixelmap_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        return await annot_connector.delete_pixelmap(
            scope_id, pixelmap_id, case_id=await es_connector.get_case_id(scope_id)
        )

    @app.put(
        "/{scope_id}/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
        tags=["data"],
        summary="Post pixelmap tile",
        status_code=204,
        responses={
            204: {},
            404: {"model": Message, "description": "Pixelmap not found"},
            422: {"model": Message, "description": "Invalid parameters"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        request: Request,
        scope_id: Id = Path(...),
        pixelmap_id: str = params.pixelmap_id,
        level: int = params.level,
        tile_x: int = params.tile_x,
        tile_y: int = params.tile_y,
        content_encoding: Annotated[str | None, Header()] = None,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        mds_url = settings.medical_data_service_url
        headers = {
            "Content-Type": "application/octet-stream",
            "Content-Encoding": content_encoding,
            "case-id": await es_connector.get_case_id(scope_id),
        }
        return await http_client.put_stream_response(
            f"{mds_url}/v3/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
            data=request.stream(),
            headers=headers,
        )

    @app.put(
        "/{scope_id}/pixelmaps/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data",
        tags=["data"],
        summary="Bulk tile upload for a pixelmap",
        status_code=204,
        responses={
            204: {},
            404: {"model": Message, "description": "Pixelmap not found"},
            413: {"model": Message, "description": "Requested data too large"},
            422: {"model": Message, "description": "Invalid parameters"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        request: Request,
        scope_id: Id = Path(...),
        pixelmap_id: Id = params.pixelmap_id,
        level: int = Path(ge=0, example=0, description="Pyramid level of region"),
        start_x: int = Path(example=0, description="Start position in x dimension"),
        start_y: int = Path(example=0, description="Start position in y dimension"),
        end_x: int = Path(example=0, description="End position in x dimension"),
        end_y: int = Path(example=0, description="End position in y dimension"),
        content_encoding: Annotated[str | None, Header()] = None,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        mds_url = settings.medical_data_service_url + "/v3/pixelmaps"
        headers = {
            "Content-Type": "application/octet-stream",
            "Content-Encoding": content_encoding,
            "case-id": await es_connector.get_case_id(scope_id),
        }
        return await http_client.put_stream_response(
            f"{mds_url}/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data",
            data=request.stream(),
            headers=headers,
        )

    @app.get(
        "/{scope_id}/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
        tags=["data"],
        summary="Get pixelmap tile",
        responses=PixelmapTileResponse,
        response_class=StreamingResponse,
    )
    async def _(
        scope_id: Id = Path(...),
        pixelmap_id: str = params.pixelmap_id,
        level: int = params.level,
        tile_x: int = params.tile_x,
        tile_y: int = params.tile_y,
        accept_encoding: Annotated[str | None, Header()] = None,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await annot_connector.fetch_pixelmap_tile(
            pixelmap_id,
            level,
            tile_x,
            tile_y,
            accept_encoding=accept_encoding,
            case_id=await es_connector.get_case_id(scope_id),
        )

    @app.get(
        "/{scope_id}/pixelmaps/{pixelmap_id}/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data",
        tags=["data"],
        summary="Bulk tile retrieval for a pixelmap",
        status_code=200,
        responses={
            200: {
                "content": {
                    "application/octet-stream": {"schema": {"type": "string", "format": "binary"}},
                },
                "description": "Pixelmap tile response.",
            },
            404: {"model": Message, "description": "Pixelmap not found"},
            413: {"model": Message, "description": "Requested data too large"},
            422: {"model": Message, "description": "Invalid parameters"},
        },
    )
    async def _(
        scope_id: Id = Path(...),
        pixelmap_id: str = params.pixelmap_id,
        level: int = Path(ge=0, example=0, description="Pyramid level of region"),
        start_x: int = Path(example=0, description="Start position in x dimension"),
        start_y: int = Path(example=0, description="Start position in y dimension"),
        end_x: int = Path(example=0, description="End position in x dimension"),
        end_y: int = Path(example=0, description="End position in y dimension"),
        accept_encoding: Annotated[str | None, Header()] = None,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await annot_connector.fetch_pixelmap_tiles(
            pixelmap_id,
            level,
            start_x,
            start_y,
            end_x,
            end_y,
            accept_encoding=accept_encoding,
            case_id=await es_connector.get_case_id(scope_id),
        )

    @app.delete(
        "/{scope_id}/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
        tags=["data"],
        summary="Delete pixelmap tile",
        status_code=204,
        responses={
            204: {},
            404: {"model": Message, "description": "Pixelmap not found"},
            423: {"model": Message, "description": "Pixelmap is locked"},
        },
    )
    async def _(
        scope_id: Id = Path(...),
        pixelmap_id: str = params.pixelmap_id,
        level: int = params.level,
        tile_x: int = params.tile_x,
        tile_y: int = params.tile_y,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        return await annot_connector.delete_pixelmap_tile(
            pixelmap_id, level, tile_x, tile_y, case_id=await es_connector.get_case_id(scope_id)
        )

    @app.get(
        "/{scope_id}/pixelmaps/{pixelmap_id}/info",
        tags=["data"],
        summary="Get level and resolution of the referenced slide",
        status_code=200,
        responses={
            200: {"model": PixelmapSlideInfo},
            404: {"model": Message, "description": "Pixelmap not found or info not available"},
        },
    )
    async def _(
        scope_id: Id = Path(...),
        pixelmap_id: str = params.pixelmap_id,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await annot_connector.get_pixelmap_info(pixelmap_id, case_id=await es_connector.get_case_id(scope_id))
