from fastapi import Body, HTTPException, Path, status

from .....models.v3.commons import Id, IdObject
from .....models.v3.job import Job, JobList, JobMode, JobStatus
from ...connectors import (
    annot_connector,
    cds_connector,
    es_connector,
    idms_connector,
    jes_connector,
    js_connector,
    mps_connector,
)
from ...custom_models.id_mapper import MappingType
from ...custom_models.jobs import PostJob
from ...custom_models.slides import JobSlideList
from ...singletons import api_integration, scope_validation, settings

ANNOTATION_TYPES = ["point", "line", "arrow", "rectangle", "circle", "polygon"]
PRIMITIVE_TYPES = ["integer", "float", "string", "bool"]


def add_routes(app, late_init):
    @app.post(
        "/{scope_id}/jobs",
        tags=["jobs"],
        summary="Create job",
        status_code=201,
        response_model=Job,
        responses={
            201: {"model": Job, "description": "A new job ready for assembly"},
            400: {"description": "App/EAD not found"},
            412: {"description": "If the job's creator_id does not match the scope_id"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope to create the job for"),
        job_data: PostJob = Body(..., description="Job creation attributes, creator_id needs to match scope_id"),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        if scope_id != job_data.creator_id:
            raise HTTPException(
                status_code=status.HTTP_412_PRECONDITION_FAILED,
                detail="The job must contain the given scope_id as creator_id",
            )
        if job_data.creator_type != "SCOPE":
            raise HTTPException(
                status_code=status.HTTP_412_PRECONDITION_FAILED, detail="The job must have SCOPE set as creator_type"
            )
        if job_data.mode == JobMode.PREPROCESSING:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail="Job mode in scope context must not be 'PREPROCESSING'"
            )
        raw_examination = await es_connector.fetch_examination_by_scope_id(scope_id)
        job_data = job_data.model_dump()
        job_data["app_id"] = raw_examination["app_id"]
        raw_job = await js_connector.create_job_raw(job_data)
        await es_connector.put_examination_job(raw_examination["id"], raw_job["id"])
        return raw_job

    @app.get(
        "/{scope_id}/jobs",
        tags=["jobs"],
        summary="Get jobs",
        responses={
            200: {
                "model": JobList,
                "description": (
                    "The list of jobs belonging to the app of the given scope in the current examination. "
                    "Furthermore jobs of the same app in the current examination but with another scope_id as long "
                    "they are not in status ASSEMBLY. This includes preprocessing jobs of the app."
                    "Note: If WBS_DISABLE_MULTI_USER is True, only jobs created by given scope and "
                    "preprocessing jobs are returned."
                ),
            },
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope to get jobs for"),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        # 1. get job ids for app_id from given scope's examination
        raw_examination = await es_connector.fetch_examination_by_scope_id(scope_id)
        job_ids = raw_examination["jobs"]
        if len(job_ids) == 0:
            return {"item_count": 0, "items": []}

        # 2. get jobs for these ids (1 request to JS)
        raw_jobs = await js_connector.query_raw_jobs(jobs=job_ids)

        # 3. filter jobs
        # if WBS_DISABLE_MULTI_USER is False (default)
        # filter out jobs in state "ASSEMBLY" that do not have this scope's id as creator_id (filter on WBS side)
        # if WBS_DISABLE_MULTI_USER is True
        # only jobs created by given scope and preprocessing jobs are returned
        jobs_to_return = []
        if settings.disable_multi_user:
            for job in raw_jobs["items"]:
                if job["creator_id"] == scope_id or (job["mode"] == "PREPROCESSING" and job["status"] != "ASSEMBLY"):
                    jobs_to_return.append(job)
        else:
            for job in raw_jobs["items"]:
                if job["status"] != "ASSEMBLY":
                    jobs_to_return.append(job)
                else:
                    if job["creator_id"] == scope_id:
                        jobs_to_return.append(job)

        return {"item_count": len(jobs_to_return), "items": jobs_to_return}

    @app.put(
        "/{scope_id}/jobs/{job_id}/inputs/{input_key}",
        tags=["jobs"],
        summary="Update input of job",
        response_model=Job,
        responses={
            200: {"model": Job, "description": "The updated job"},
            400: {"description": "Job was not found, input_key is not valid or input resource not valid"},
            412: {"description": "The job does not belong to the given scope"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job that should be updated"),
        input_key: str = Path(..., description="Identifier for the input to update"),
        input_id_obj: IdObject = Body(..., description="An object containing the ID of the given input"),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        job = await get_scope_validated_job(job_id, scope_id)
        ead = await mps_connector.fetch_raw_ead(job["app_id"])
        validate_input_key(input_key, job["mode"], ead)
        return await js_connector.update_job_input_raw(job["id"], input_key, input_id_obj.id)

    @app.delete(
        "/{scope_id}/jobs/{job_id}/inputs/{input_key}",
        tags=["jobs"],
        summary="Remove input from job",
        response_model=Job,
        responses={
            200: {"model": Job, "description": "The updated job"},
            400: {"description": "Job was not found or input_key is not valid"},
            412: {"description": "The job does not belong to the given scope"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job that should be updated"),
        input_key: str = Path(..., description="Identifier for the input to remove"),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        job = await get_scope_validated_job(job_id, scope_id)
        return await js_connector.delete_job_input_raw(job["id"], input_key)

    @app.put(
        "/{scope_id}/jobs/{job_id}/outputs/{output_key}",
        tags=["jobs"],
        summary="Update output of job",
        response_model=Job,
        responses={
            200: {"model": Job, "description": "The updated job"},
            400: {"description": "Job was not found, output_key is not valid or input resource not valid"},
            412: {"description": "The job does not belong to the given scope"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job that should be updated"),
        output_key: str = Path(..., description="Identifier for the output to update"),
        output_id_obj: IdObject = Body(..., description="An object containing the ID of the given input"),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        job = await get_scope_validated_job(job_id, scope_id)
        if job["mode"] not in [JobMode.REPORT, JobMode.POSTPROCESSING]:
            raise HTTPException(status_code=400, detail=f"Updating outputs for job mode {job['mode']} not allowed")
        if job["containerized"]:
            raise HTTPException(
                status_code=status.HTTP_412_PRECONDITION_FAILED,
                detail="Outputs of containerized jobs can only be added by the container.",
            )
        ead = await mps_connector.fetch_raw_ead(job["app_id"])
        validate_output_key(output_key, job["mode"], ead)
        return await js_connector.update_job_output_raw(job["id"], output_key, output_id_obj.id)

    @app.delete(
        "/{scope_id}/jobs/{job_id}/outputs/{output_key}",
        tags=["jobs"],
        summary="Update output of job",
        response_model=Job,
        responses={
            200: {"model": Job, "description": "The updated job"},
            400: {"description": "Job was not found, output_key is not valid or input resource not valid"},
            412: {"description": "The job does not belong to the given scope"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job that should be updated"),
        output_key: str = Path(..., description="Identifier for the output to update"),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        job = await get_scope_validated_job(job_id, scope_id)
        if job["mode"] not in [JobMode.REPORT, JobMode.POSTPROCESSING]:
            raise HTTPException(status_code=400, detail=f"Deleting outputs for job mode {job['mode']} not allowed")
        if job["containerized"]:
            raise HTTPException(
                status_code=status.HTTP_412_PRECONDITION_FAILED,
                detail="Can only remove outputs of non-containerized outputs.",
            )
        return await js_connector.delete_job_output_raw(job["id"], output_key)

    @app.delete(
        "/{scope_id}/jobs/{job_id}",
        tags=["jobs"],
        summary="Remove job",
        response_model=Job,
        responses={
            200: {"model": Job, "description": "The deleted job"},
            400: {"description": "Job not found or not in assembly state"},
            412: {"description": "The job does not belong to the given scope"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job that should be updated"),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        raw_examination = await es_connector.validate_examination_state_open(scope_id=scope_id)
        job = await get_scope_validated_job(job_id, scope_id)
        await js_connector.delete_job_raw(job_id=job["id"])
        await es_connector.delete_examination_job(ex_id=raw_examination["id"], job_id=job["id"])
        return job

    @app.get(
        "/{scope_id}/jobs/{job_id}",
        tags=["jobs"],
        summary="Get job",
        response_model=Job,
        responses={
            200: {"model": Job, "description": "The job for given scope"},
            400: {"description": "Job not found"},
            412: {"description": "The job does not belong to the given scope"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job"),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await get_scope_validated_job(job_id, scope_id)

    @app.put(
        "/{scope_id}/jobs/{job_id}/run",
        tags=["jobs"],
        summary="Run job",
        response_model=Job,
        responses={
            200: {"model": Job, "description": "The scheduled or failed job"},
            400: {"description": "Job cannot be executed"},
            412: {"description": "The job does not belong to the given scope"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job that should be executed"),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        job = await get_scope_validated_job(job_id, scope_id)
        if not job["containerized"]:
            raise HTTPException(status_code=400, detail="Only containerized jobs can be run.")
        ead = await mps_connector.fetch_raw_ead(job["app_id"])
        case_id = await es_connector.get_case_id(scope_id)
        # get ids of classes indirectly in job (e.g. class constraints)
        class_ids = await get_job_class_ids(job, ead, case_id=case_id)
        await lock_io(job, "inputs", ead, class_ids, case_id=case_id)
        result = await execute_job(job)
        late_init.daemon_tick()
        return result

    @app.put(
        "/{scope_id}/jobs/{job_id}/finalize",
        tags=["jobs"],
        summary="Finalize job",
        response_model=Job,
        responses={
            200: {"model": Job, "description": "The finalized job"},
            400: {"description": "Job not found or invalid status transition"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job that should be executed"),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        job = await get_scope_validated_job(job_id, scope_id)
        if job["containerized"]:
            raise HTTPException(
                status_code=status.HTTP_412_PRECONDITION_FAILED,
                detail="Containerized jobs can only be finalized by the container.",
            )
        if job["mode"] == "REPORT":
            raise HTTPException(
                status_code=status.HTTP_412_PRECONDITION_FAILED,
                detail="Report jobs are automatically finalized when closing the examination",
            )
        return await finalize_job(job, case_id=await es_connector.get_case_id(scope_id))

    @app.put(
        "/{scope_id}/jobs/{job_id}/stop",
        tags=["jobs"],
        summary="Stop job",
        status_code=200,
        responses={
            200: {"model": bool},
            400: {"description": "Error stoping job"},
            404: {"description": "Job Execution not found"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job that should be executed"),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        job = await get_scope_validated_job(job_id, scope_id)
        if not job["containerized"]:
            raise HTTPException(status_code=400, detail="Only containerized jobs can be stopped.")
        return await jes_connector.stop_job_execution(job["id"])

    @app.get(
        "/{scope_id}/jobs/{job_id}/slides",
        tags=["jobs"],
        summary="Get slides used in a given job",
        responses={
            200: {"model": JobSlideList, "description": "The list of slides used in the given job"},
            400: {"description": "Job was not found"},
            412: {"description": "The job does not belong to the given scope"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job that should be executed"),
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        job = await get_scope_validated_job(job_id, scope_id)
        tags = await cds_connector.fetch_raw_tags()
        ead = await mps_connector.fetch_raw_ead(job["app_id"])
        slide_ids = await get_slide_ids_for_job(job, ead, case_id=await es_connector.get_case_id(scope_id))
        slides = []
        for slide_id in slide_ids:
            slide = await cds_connector.fetch_raw_slide(slide_id)
            slide["mds_url"], slide["local_id"] = await idms_connector.fetch_mapping(slide_id, MappingType.SLIDES)
            slide["stain"], slide["tissue"] = await cds_connector.get_raw_stain_and_tissue_mapping(slide, tags)
            slides.append(slide)
        return JobSlideList(items=slides, item_count=len(slides))


async def finalize_job(job: dict, target_status="COMPLETED", error_message=None, case_id=None):
    ead = await mps_connector.fetch_raw_ead(job["app_id"])
    # get ids of classes indirectly in job (e.g. class constraints)
    class_ids_in = await get_job_class_ids(job, ead, case_id=case_id)
    # lock io in ead AND indirect classes
    await lock_io(job, "inputs", ead, class_ids_in, case_id=case_id)
    # lock io in ead
    await lock_io(job, "outputs", ead, case_id=case_id)
    # job complete
    job = await js_connector.update_job_status_raw(job["id"], target_status, error_message)
    return job


async def get_scope_validated_job(job_id, scope_id):
    job = await js_connector.fetch_raw_job(job_id)
    raw_examination = await es_connector.fetch_examination_by_scope_id(scope_id)
    if job_id not in raw_examination["jobs"]:
        raise HTTPException(
            status_code=status.HTTP_412_PRECONDITION_FAILED, detail="The job does not belong to the given scope"
        )
    return job


def validate_input_key(input_key, mode, ead):
    mode = mode.lower()
    if not ead["io"].get(input_key):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="The given input_key is invalid")
    if mode not in ead["modes"]:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="The given mode is invalid")
    if input_key not in ead["modes"][mode]["inputs"]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="The given input_key is invalid for given mode"
        )


def validate_output_key(output_key, mode, ead):
    mode = mode.lower()
    if not ead["io"].get(output_key):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="The given output_key is invalid")
    if mode not in ead["modes"]:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="The given mode is invalid")
    if output_key not in ead["modes"][mode]["outputs"]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="The given output_key is invalid for given mode"
        )


async def get_job_class_ids(job, ead, case_id=None):
    classes_to_lock = []
    for key, value in ead["io"].items():
        if key in ead["modes"][job["mode"].lower()]["inputs"]:
            if value["type"] == "class":
                return None
            if value["type"] == "collection":
                if is_class_collection(value["items"]):
                    return None
            if key in job["inputs"]:
                temp_classes = await get_class_ids_recursive(ead["io"][key], job["inputs"][key], case_id=case_id)
                classes_to_lock.extend(temp_classes)
    return classes_to_lock


def is_class_collection(items):
    if items["type"] == "class":
        return True
    elif items["type"] == "collection":
        return is_class_collection(items["items"])
    else:
        return False


def get_ead_class_values_recursive(base_name: str, class_key, class_node):
    base_name = f"{base_name}.{class_key}"
    class_values = []
    if "name" in class_node:
        class_values.append(base_name)
    else:
        for key, value in class_node.items():
            class_values.extend(get_ead_class_values_recursive(base_name, key, value))
    return class_values


async def get_class_ids_recursive(io_item, io_id, case_id=None):
    class_ids = []
    if "classes" in io_item:
        annotation = await annot_connector.fetch_raw_annotation(io_id, with_classes=True, case_id=case_id)
        for annot_class in annotation["classes"]:
            for cls in io_item["classes"]:
                if annot_class["value"].startswith(cls):
                    class_ids.append(annot_class["id"])
    elif io_item["type"] == "collection":
        collection = await annot_connector.fetch_raw_collection_items(io_id, case_id=case_id)
        for item in collection["items"]:
            ids = await get_class_ids_recursive(io_item["items"], item["id"], case_id=case_id)
            class_ids.extend(ids)
    return class_ids


async def lock_io(job, input_output, ead, class_ids=None, case_id=None):
    for io_key in job[input_output]:
        io_id = job[input_output][io_key]
        io_type = ead["io"][io_key]["type"]
        if io_type in ANNOTATION_TYPES:
            await annot_connector.lock_annotation(io_id, job["id"], case_id=case_id)
        elif io_type in PRIMITIVE_TYPES:
            await annot_connector.lock_primitive(io_id, job["id"], case_id=case_id)
        elif io_type == "class":
            await annot_connector.lock_class(io_id, job["id"], case_id=case_id)
        elif io_type == "collection":
            await annot_connector.lock_collection(io_id, job["id"], case_id=case_id)
    if class_ids:
        for class_id in class_ids:
            await annot_connector.lock_class(class_id, job["id"], case_id=case_id)


async def execute_job(job):
    return await js_connector.update_job_status_raw(job["id"], JobStatus.READY)


async def get_slide_ids_for_job(job, ead, case_id=None):
    slide_ids = []
    for input_key, input_id in job["inputs"].items():
        input_spec = ead["io"][input_key]
        if input_spec["type"] == "wsi":
            slide_ids.append(input_id)
        elif is_wsi_collection(input_spec):
            collection = await annot_connector.fetch_raw_collection(input_id, shallow=False, case_id=case_id)
            slide_ids.extend(flatten_collection_item_ids(collection))
    return slide_ids


def is_wsi_collection(spec):
    while spec["type"] == "collection":
        spec = spec["items"]
    return spec["type"] == "wsi"


def flatten_collection_item_ids(collection):
    items = []
    if collection["item_type"] == "collection":
        for nested in collection["items"]:
            items.extend(flatten_collection_item_ids(nested))
        return items
    return [item["id"] for item in collection["items"]]
