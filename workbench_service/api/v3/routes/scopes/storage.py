from fastapi import Response

from .....models.v3.commons import Id, Message
from .....models.v3.examination import AppUiStorage
from ...connectors import es_connector
from ...singletons import api_integration, scope_validation


def add_routes(app, late_init):
    @app.get(
        "/{scope_id}/app-ui-storage/user",
        tags=["storage"],
        summary="Get app ui storage for user of given scope",
        responses={
            200: {"model": AppUiStorage},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        scope_id: Id,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await es_connector.get_app_ui_storage_user(scope_id)

    @app.get(
        "/{scope_id}/app-ui-storage/scope",
        tags=["storage"],
        summary="Get app ui storage for scope",
        responses={
            200: {"model": AppUiStorage},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        scope_id: Id,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        return await es_connector.get_app_ui_storage_scope(scope_id)

    @app.put(
        "/{scope_id}/app-ui-storage/user",
        tags=["storage"],
        summary="Put app ui storage for user of given scope",
        status_code=201,
        responses={
            201: {"model": AppUiStorage},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        response: Response,
        scope_id: Id,
        storage: AppUiStorage,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        status, data = await es_connector.put_app_ui_storage_user(scope_id, storage)
        response.status_code = status
        return data

    @app.put(
        "/{scope_id}/app-ui-storage/scope",
        tags=["storage"],
        summary="Put app ui storage for scope",
        status_code=201,
        responses={
            201: {"model": AppUiStorage},
            404: {
                "model": Message,
                "description": "The resource was not found",
            },
        },
    )
    async def _(
        response: Response,
        scope_id: Id,
        storage: AppUiStorage,
        payload=scope_validation.scope_depends(),
    ):
        await api_integration.scope_hook(scope_id, payload)
        status, data = await es_connector.put_app_ui_storage_scope(scope_id, storage)
        response.status_code = status
        return data
