from . import frontends


def add_routes(app, late_init):
    frontends.add_routes(app, late_init)
