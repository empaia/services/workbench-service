from fastapi import Header

from ....models.marketplace.app import TagList
from ..connectors import mps_connector
from ..custom_models.apps import AppList, AppQuery
from ..singletons import api_integration


def add_routes(app, late_init):
    @app.put(
        "/apps/query",
        tags=["apps"],
        response_model=AppList,
    )
    async def _(
        query: AppQuery,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        query_to_mps = query.model_dump()
        modes = query_to_mps.pop("job_modes", None)
        return await mps_connector.fetch_aggregated_apps(query=query_to_mps, job_modes=modes)

    @app.get(
        "/tags",
        tags=["tags"],
        response_model=TagList,
    )
    async def _(
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        # just tags, no auth hook check
        return await mps_connector.fetch_raw_tags()
