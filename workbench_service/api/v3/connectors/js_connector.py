from typing import List

from ....models.v3.commons import Id
from ....models.v3.job import JobMode
from ....singletons import http_client, mds_url


async def create_job_raw(job_data):
    url = f"{mds_url}/v3/jobs"
    job = await http_client.post(url, json=job_data)
    return job


async def query_raw_jobs(
    jobs: List[str] = None,
    apps: List[str] = None,
    creators: List[str] = None,
    modes: List[JobMode] = None,
    skip: int = None,
    limit: int = None,
) -> dict:
    url = f"{mds_url}/v3/jobs/query"
    raw_query = {}
    if jobs:
        raw_query["jobs"] = jobs
    if apps:
        raw_query["apps"] = apps
    if creators:
        raw_query["creators"] = creators
    if modes:
        raw_query["modes"] = modes
    params = {}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    raw_jobs = await http_client.put(url, json=raw_query, params=params)
    return raw_jobs


async def fetch_raw_job(job_id: Id):
    url = f"{mds_url}/v3/jobs/{job_id}"
    return await http_client.get(url)


async def fetch_raw_jobs_by_creator(creator_id: Id):
    raw_query = {"creators": [creator_id]}
    url = f"{mds_url}/v3/jobs/query"
    return await http_client.put(url, json=raw_query)


async def update_job_input_raw(job_id, input_key: str, input_id: Id):
    url = f"{mds_url}/v3/jobs/{job_id}/inputs/{input_key}"
    return await http_client.put(url, json={"id": input_id})


async def update_job_output_raw(job_id, output_key: str, output_d: Id):
    url = f"{mds_url}/v3/jobs/{job_id}/outputs/{output_key}"
    return await http_client.put(url, json={"id": output_d})


async def update_job_status_raw(job_id, status: str, error_message: str = None):
    url = f"{mds_url}/v3/jobs/{job_id}/status"
    payload = {"status": status}
    if error_message:
        payload["error_message"] = error_message
    return await http_client.put(url, json=payload)


async def delete_job_input_raw(job_id, input_key: str):
    url = f"{mds_url}/v3/jobs/{job_id}/inputs/{input_key}"
    return await http_client.delete(url)


async def delete_job_output_raw(job_id, output_key: str):
    url = f"{mds_url}/v3/jobs/{job_id}/outputs/{output_key}"
    return await http_client.delete(url)


async def delete_job_raw(job_id):
    url = f"{mds_url}/v3/jobs/{job_id}"
    return await http_client.delete(url)
