from typing import List, Tuple

from fastapi.exceptions import HTTPException

from workbench_service.api.v3.custom_models.apps import App

from ....models.marketplace.app import ApiVersion
from ....models.v3.commons import Id
from ....models.v3.examination import AppUiStorage, PreprocessingTrigger, ProprocessingTriggerCreatorType
from ....models.v3.job import Job, JobMode
from ....singletons import http_client, mds_url, settings, v3_scope_case_cache
from ..custom_models.cases import PreprocessingProgress
from ..custom_models.examinations import Examination, ExaminationList, ExaminationState, PostPreprocessingTrigger
from ..custom_models.jobs import JOB_FINISHED_STATES, JOB_UNFINISHED_STATES
from . import js_connector, mps_connector


async def aggregate_examination(raw_examination: dict, app: App, raw_jobs: dict) -> Examination:
    jobs_count_finished = 0
    jobs = []

    if raw_jobs is not None:
        for raw_job in raw_jobs["items"]:
            jobs.append(Job.model_validate(raw_job))

            if raw_job["status"] in JOB_FINISHED_STATES:
                jobs_count_finished += 1

    examination = Examination(
        id=raw_examination["id"],
        case_id=raw_examination["case_id"],
        creator_id=raw_examination["creator_id"],
        creator_type=raw_examination["creator_type"],
        state=raw_examination["state"],
        created_at=raw_examination["created_at"],
        updated_at=raw_examination["updated_at"],
        jobs=jobs,
        jobs_count=len(jobs),
        jobs_count_finished=jobs_count_finished,
        app_id=raw_examination["app_id"],
        app=app,
        api_version=ApiVersion.V3,
    )
    return examination


async def fetch_raw_examination(examination_id: str) -> dict:
    url = f"{mds_url}/v3/examinations/{examination_id}"
    raw_examination = await http_client.get(url)
    return raw_examination


async def fetch_jobs(examination_id: str, app_id: str) -> List[str]:
    raw_examination = await fetch_raw_examination(examination_id=examination_id)
    for app in raw_examination["apps"]:
        if app["id"] == app_id:
            return app["jobs"]
    return None


async def fetch_examination(examination_id: str) -> Examination:
    raw_examination = await fetch_raw_examination(examination_id=examination_id)
    app = await mps_connector.fetch_aggregated_app_by_app_id(raw_examination["app_id"])
    raw_jobs = None
    if len(raw_examination["jobs"]) > 0:
        raw_jobs = await js_connector.query_raw_jobs(jobs=raw_examination["jobs"])
    examination = await aggregate_examination(raw_examination=raw_examination, app=app, raw_jobs=raw_jobs)
    return examination


async def fetch_raw_examinations(cases: List[str] = None, skip: int = None, limit: int = None) -> dict:
    raw_query = {}
    if cases:
        raw_query["cases"] = cases
    return await query_raw_examinations(raw_query, skip, limit)


async def fetch_examination_by_scope_id(scope_id: str):
    raw_exs = await query_raw_examinations({"scopes": [scope_id]})
    if raw_exs["item_count"] == 0:
        raise HTTPException(
            detail=f"No examination for scope_id {scope_id} found.",
            status_code=400,
        )
    if raw_exs["item_count"] > 1:
        raise HTTPException(
            detail=f"Inconsistent data. Multiple examinations found for scope_id {scope_id} found.",
            status_code=400,
        )
    return raw_exs["items"][0]


async def query_raw_examinations(query: dict, skip: int = None, limit: int = None) -> dict:
    url = f"{mds_url}/v3/examinations/query"
    params = {}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    raw_examinations = await http_client.put(url, json=query, params=params)
    return raw_examinations


async def fetch_examinations(cases: List[str] = None, skip: int = None, limit: int = None) -> ExaminationList:
    raw_examinations = await fetch_raw_examinations(cases=cases, skip=skip, limit=limit)
    return await aggregate_examinations(raw_examinations=raw_examinations)


async def aggregate_examinations(raw_examinations: dict):
    examination_jobs_mapping = {}
    job_ids = []
    for raw_examination in raw_examinations["items"]:
        if "jobs" in raw_examination and len(raw_examination["jobs"]) > 0:
            examination_jobs_mapping[raw_examination["id"]] = raw_examination["jobs"]
            job_ids.extend(raw_examination["jobs"])

    apps = await mps_connector.fetch_aggregated_apps(query={}, job_modes=None)
    raw_jobs = await js_connector.query_raw_jobs(jobs=job_ids)

    items = []
    for raw_examination in raw_examinations["items"]:
        app = next((app for app in apps.items if app.app_id == raw_examination["app_id"]), None)
        if not app:
            continue

        relevant_raw_jobs = {"items": [], "item_count": 0}
        if raw_examination["id"] in examination_jobs_mapping:
            for raw_job in raw_jobs["items"]:
                if str(raw_job["id"]) in examination_jobs_mapping[raw_examination["id"]]:
                    relevant_raw_jobs["items"].append(raw_job)

        relevant_raw_jobs["item_count"] = len(relevant_raw_jobs["items"])

        examination = await aggregate_examination(raw_examination=raw_examination, app=app, raw_jobs=relevant_raw_jobs)
        items.append(examination)

    examinations = ExaminationList(items=items, item_count=raw_examinations["item_count"])
    return examinations


async def get_preprocessing_progress(raw_examinations: dict):
    job_ids = []
    for raw_examination in raw_examinations["items"]:
        job_ids += raw_examination["jobs"]

    preprocessing_progress = PreprocessingProgress.NONE

    if len(job_ids) > 0:
        raw_jobs = await js_connector.query_raw_jobs(jobs=job_ids, modes=[JobMode.PREPROCESSING])

        if raw_jobs and len(raw_jobs["items"]) > 0:
            preprocessing_progress = PreprocessingProgress.FINISHED

            for raw_job in raw_jobs["items"]:
                if raw_job["status"] in JOB_UNFINISHED_STATES:
                    preprocessing_progress = PreprocessingProgress.RUNNING
                    break

    return preprocessing_progress


async def get_preprocessing_progress_dict(cases_jobs_mapping: dict):
    job_list_query = []
    for job_ids in cases_jobs_mapping.values():
        if job_ids and len(job_ids) > 0:
            job_list_query.extend(job_ids)

    raw_jobs = await js_connector.query_raw_jobs(jobs=job_list_query, modes=[JobMode.PREPROCESSING])

    preprocessing_progress_dict = {}
    for case_id in cases_jobs_mapping.keys():
        job_ids = cases_jobs_mapping[case_id]
        if job_ids and len(job_ids) > 0:
            preprocessing_progress_dict[case_id] = PreprocessingProgress.FINISHED
            for raw_job in raw_jobs["items"]:
                if str(raw_job["id"]) in job_ids and raw_job["status"] in JOB_UNFINISHED_STATES:
                    preprocessing_progress_dict[case_id] = PreprocessingProgress.RUNNING
                    break
        else:
            preprocessing_progress_dict[case_id] = PreprocessingProgress.NONE

    return preprocessing_progress_dict


async def fetch_or_create_raw_open_examination(case_id: str, app_id: str, user_id: str) -> dict:
    url = f"{mds_url}/v3/examinations"
    payload = {"case_id": case_id, "app_id": app_id, "creator_id": user_id, "creator_type": "USER"}
    status, raw_examination = await http_client.put(url, json=payload, return_status=True)
    return status, raw_examination


async def put_examination_state(examination_id: str, state: ExaminationState) -> dict:
    url = f"{mds_url}/v3/examinations/{examination_id}/state"
    payload = {"state": state.value}
    raw_examination = await http_client.put(url, json=payload)
    return raw_examination


def check_examination_jobs_finished(ex: Examination, exclude_report):
    for job in ex.jobs:
        if exclude_report and job.mode != JobMode.REPORT:
            if job.status not in JOB_FINISHED_STATES:
                raise HTTPException(409, f"At least one job in examination '{ex.id}' is in a not finished state")


async def fetch_or_create_raw_scope(examination_id: str, user_id: str) -> Tuple[int, dict]:
    url = f"{mds_url}/v3/scopes"
    payload = {"examination_id": examination_id, "user_id": user_id}
    raw_scope = None
    try:
        raw_scope = await http_client.put(url, json=payload)
        status_code = 200
    except HTTPException as e:
        if e.status_code != 404:
            raise
    if raw_scope is None:
        try:
            raw_scope = await http_client.post(url, json=payload)
            status_code = 201
        except HTTPException as e:
            # in case scope creation fails due to parallel requests
            if e.status_code == 400:
                raw_scope = await http_client.put(url, json=payload)
                status_code = 200
            else:
                raise
    return status_code, raw_scope


async def fetch_raw_scope_token(scope_id: Id) -> dict:
    url = f"{mds_url}/v3/scopes/{scope_id}/token"
    return await http_client.get(url)


async def fetch_raw_scope_data(scope_id: Id) -> dict:
    url = f"{mds_url}/v3/scopes/{scope_id}"
    return await http_client.get(url)


async def query_scopes(ex_id) -> dict:
    query = {"examinations": [str(ex_id)]}
    url = f"{mds_url}/v3/scopes/query"
    return await http_client.put(url, json=query)


async def fetch_raw_public_key() -> bytes:
    url = f"{mds_url}/v3/examinations/public-key"
    status_code, public_key = await http_client.get(url, return_status=True)
    if status_code < 400:
        public_key = public_key.encode("ascii")
    else:
        raise HTTPException(status_code=status_code, detail="Failed to retrieve public-key from examination-service")
    return public_key


async def fetch_case_id_for_scope(scope_data: dict) -> str:
    raw_examination = await fetch_raw_examination(scope_data["examination_id"])
    return raw_examination["case_id"]


async def put_examination_job(ex_id, job_id):
    return await http_client.put(
        f"{mds_url}/v3/examinations/{ex_id}/jobs/{job_id}/add",
        None,
    )


async def delete_examination_job(ex_id, job_id):
    url = f"{mds_url}/v3/examinations/{ex_id}/jobs/{job_id}"
    return await http_client.delete(url)


async def validate_examination_state_open(scope_id):
    raw_examination = await fetch_examination_by_scope_id(scope_id)
    if raw_examination["state"] == ExaminationState.CLOSED:
        raise HTTPException(
            status_code=423, detail="Examination has state 'CLOSED': Create or delete actions are not allowed"
        )
    return raw_examination


async def post_preprocessing_trigger(
    preprocessing_trigger: PostPreprocessingTrigger, user_id: str
) -> PreprocessingTrigger:
    preprocessing_trigger_dict = preprocessing_trigger.model_dump()
    preprocessing_trigger_dict.update({"creator_id": user_id, "creator_type": ProprocessingTriggerCreatorType.USER})
    result = await http_client.post(f"{mds_url}/v3/preprocessing-triggers", json=preprocessing_trigger_dict)
    return await extend_preprocessing_trigger(result)


async def get_preprocessing_triggers():
    result = await http_client.get(f"{mds_url}/v3/preprocessing-triggers")
    return result


async def get_extended_preprocessing_triggers():
    preprocessing_triggers = await get_preprocessing_triggers()
    apps = await mps_connector.fetch_aggregated_apps(query={}, job_modes=None)
    for i, value in enumerate(preprocessing_triggers["items"]):
        for app in apps.items:
            if str(app.portal_app_id) == value["portal_app_id"]:
                preprocessing_triggers["items"][i].update({"app": app.model_dump()})
                break
    return preprocessing_triggers


async def get_preprocessing_trigger(preprocessing_trigger_id: str):
    result = await http_client.get(f"{mds_url}/v3/preprocessing-triggers/{preprocessing_trigger_id}")
    return result


async def get_extended_preprocessing_trigger(preprocessing_trigger_id: str):
    preprocessing_trigger = await get_preprocessing_trigger(preprocessing_trigger_id=preprocessing_trigger_id)
    return await extend_preprocessing_trigger(preprocessing_trigger)


async def extend_preprocessing_trigger(preprocessing_trigger):
    if preprocessing_trigger:
        app = await mps_connector.fetch_aggregated_app_by_portal_app_id(preprocessing_trigger["portal_app_id"])
        if app:
            preprocessing_trigger.update({"app": app})
    return preprocessing_trigger


async def delete_preprocessing_trigger(preprocessing_trigger_id: str):
    preprocessing_trigger = await http_client.delete(f"{mds_url}/v3/preprocessing-triggers/{preprocessing_trigger_id}")
    return await extend_preprocessing_trigger(preprocessing_trigger)


async def put_app_ui_storage_scope(scope_id: Id, storage: AppUiStorage):
    url = f"{mds_url}/v3/scopes/{scope_id}/app-ui-storage/scope"
    status, data = await http_client.put(url, json=storage.model_dump(), return_status=True)
    return status, AppUiStorage.model_validate(data)


async def put_app_ui_storage_user(scope_id: Id, storage: AppUiStorage):
    url = f"{mds_url}/v3/scopes/{scope_id}/app-ui-storage/user"
    status, data = await http_client.put(url, json=storage.model_dump(), return_status=True)
    return status, AppUiStorage.model_validate(data)


async def get_app_ui_storage_scope(scope_id: Id) -> AppUiStorage:
    url = f"{mds_url}/v3/scopes/{scope_id}/app-ui-storage/scope"
    data = await http_client.get(url)
    return AppUiStorage.model_validate(data)


async def get_app_ui_storage_user(scope_id: Id) -> AppUiStorage:
    url = f"{mds_url}/v3/scopes/{scope_id}/app-ui-storage/user"
    data = await http_client.get(url)
    return AppUiStorage.model_validate(data)


async def get_case_id(scope_id: Id):
    if not settings.enable_annot_case_data_partitioning:
        return None

    case_id = v3_scope_case_cache.get(scope_id)
    if case_id is not None:
        return case_id

    scope_data = await fetch_raw_scope_data(scope_id)
    raw_examination = await fetch_raw_examination(scope_data["examination_id"])
    case_id = raw_examination["case_id"]
    v3_scope_case_cache.add(scope_id, case_id)
    return case_id
