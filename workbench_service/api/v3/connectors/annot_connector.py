from typing import List, Optional, get_args
from xmlrpc.client import boolean

from fastapi.exceptions import HTTPException

from ....models.v3.annotation.annotations import AnnotationQuery, PostAnnotations
from ....models.v3.annotation.classes import ClassQuery, PostClasses
from ....models.v3.annotation.collections import (
    CollectionQuery,
    ItemQuery,
    PostCollection,
    PostItemList,
    PostItems,
    SlideItem,
)
from ....models.v3.annotation.pixelmaps import PixelmapQuery, PostPixelmaps
from ....models.v3.annotation.primitives import PostPrimitives, PrimitiveQuery
from ....models.v3.commons import DataCreatorType, Id, IdObject
from ....singletons import http_client, mds_url, settings, v3_ex_job_cache, v3_ex_scopes_cache, v3_scope_ex_cache
from ..connectors import es_connector, mps_connector
from ..connectors.es_connector import query_scopes
from ..custom_models.annotations import ROI_CLASS_VALUE
from ..namespace_validation import get_ead_namespace, get_global_namespaces, validate_class_value


# GENERAL
async def validate_class_namespace(scope_id: Id, post_data: PostClasses) -> bool:
    raw_ex = await es_connector.fetch_examination_by_scope_id(scope_id)
    raw_ead = await mps_connector.fetch_raw_ead(raw_ex["app_id"])
    namespaces = get_global_namespaces()
    ead_namespace = get_ead_namespace(raw_ead)
    namespaces.update(ead_namespace)
    if hasattr(post_data, "items") and post_data.items is not None:
        for item in post_data.items:
            validate_class_value(item.value, namespaces)
    else:
        validate_class_value(post_data.value, namespaces)


async def validate_query(scope_id: Id, query: dict, item_type: str):
    if query.creators and query.jobs:
        raise HTTPException(status_code=400, detail="Cannot query both creators and jobs")

    ex_id = v3_scope_ex_cache.get(scope_id)
    if not ex_id:
        scope_data = await es_connector.fetch_raw_scope_data(scope_id)
        ex_id = scope_data["examination_id"]
        v3_scope_ex_cache.add(scope_id, ex_id)

    valid = False

    # validate creators
    if query.creators:
        valid = await validate_creators(ex_id, scope_id, query.creators)

    # validate jobs (only jobs in scope from same app)
    if query.jobs:
        jobs = v3_ex_job_cache.get(ex_id)
        if jobs and set(query.jobs).issubset(jobs):
            valid = True
        else:
            ex = await es_connector.fetch_raw_examination(examination_id=ex_id)
            jobs = ex["jobs"]
            v3_ex_job_cache.add(ex_id, jobs)
            valid = await validate_jobs(query.jobs, jobs)

    # validate items
    if item_type != "collections" and query.__fields_set__ == {item_type}:
        valid = True

    if not valid:
        if item_type != "collections":
            error_msg = f"Either a valid creator_id or job list or a list of {item_type} must be set as query parameter"
        else:
            error_msg = "Either a valid creator_id or job list must be set as query parameter"
        raise HTTPException(
            status_code=400,
            detail=error_msg,
        )


async def validate_creators(ex_id: Id, scope_id: Id, queried_creators: dict) -> bool:
    if settings.disable_multi_user:
        if set(queried_creators) != set([scope_id]):
            return False
    else:
        valid_scope_ids = v3_ex_scopes_cache.get(ex_id)
        if valid_scope_ids and set(queried_creators).issubset(valid_scope_ids):
            return True
        else:
            valid_scopes = await query_scopes(ex_id)
            valid_scope_ids = [scope["id"] for scope in valid_scopes["items"]]
            v3_ex_scopes_cache.add(ex_id, valid_scope_ids)
            if not set(queried_creators).issubset(valid_scope_ids):
                raise HTTPException(status_code=412, detail="Invalid creator list query parameter for current scope")
    return True


async def validate_jobs(queried_jobs: dict, valid_jobs: List[str]):
    queried_job_ids = set(queried_jobs)

    if not queried_job_ids.issubset(valid_jobs):
        raise HTTPException(status_code=412, detail="Invalid job list query parameter for current scope")

    return True


async def validate_post_data(scope_id: Id, post_data):
    if hasattr(post_data, "items") and post_data.items is not None:
        for item in post_data.items:
            if item and not isinstance(item, IdObject):
                await validate_post_data(scope_id, item)

    if hasattr(post_data, "creator_id") and post_data.creator_id != scope_id:
        raise HTTPException(status_code=412, detail="Creator_id must be set to scope_id")
    if hasattr(post_data, "creator_type") and post_data.creator_type != DataCreatorType.SCOPE:
        raise HTTPException(status_code=412, detail="Creator_type must be set to scope")


# ANNOTATIONS


async def fetch_annotations(
    annotation_query: AnnotationQuery,
    with_classes: bool,
    with_low_npp_centroids: bool,
    skip: int = None,
    limit: int = None,
    case_id: str = None,
) -> dict:
    url = f"{mds_url}/v3/annotations/query"
    raw_query = annotation_query.model_dump()
    params = {}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    if with_classes:
        params["with_classes"] = with_classes
    if with_low_npp_centroids:
        params["with_low_npp_centroids"] = with_low_npp_centroids
    headers = {"case-id": case_id}
    stream_annotations = await http_client.put_stream_response(url, params=params, json=raw_query, headers=headers)
    return stream_annotations


async def fetch_raw_annotation(annotation_id: str, with_classes: bool = False, case_id: str = None) -> dict:
    url = f"{mds_url}/v3/annotations/{annotation_id}"
    params = {}
    if with_classes:
        params["with_classes"] = with_classes
    headers = {"case-id": case_id}
    raw_annotation = await http_client.get(url, params=params, headers=headers)
    return raw_annotation


async def fetch_annotations_route_suffix(query: dict, route_suffix: str, case_id: str = None) -> dict:
    url = f"{mds_url}/v3/annotations/query/{route_suffix}"
    headers = {"case-id": case_id}
    stream_annotations = await http_client.put_stream_response(url, json=query, headers=headers)
    return stream_annotations


async def fetch_annotation_query_position(query: AnnotationQuery, annotation_id: str, case_id: str = None) -> dict:
    url = f"{mds_url}/v3/annotations/{annotation_id}/query"
    raw_query = query.model_dump()
    headers = {"case-id": case_id}
    stream_annotations = await http_client.put_stream_response(url, json=raw_query, headers=headers)
    return stream_annotations


async def post_annotations(annotations: PostAnnotations, external_ids: boolean = False, case_id: str = None) -> dict:
    annotations_dict = annotations.model_dump()
    url = f"{mds_url}/v3/annotations"
    params = {}
    if external_ids:
        params["external_ids"] = external_ids
    headers = {"case-id": case_id}
    raw_annotations = await http_client.post(url, json=annotations_dict, params=params, headers=headers)
    return raw_annotations


async def delete_annotation(scope_id: Id, annotation_id: str, case_id: str = None) -> dict:
    annotration = await fetch_raw_annotation(annotation_id, case_id=case_id)
    if annotration["creator_id"] != scope_id:
        raise HTTPException(status_code=412, detail="Not allowed to delete annotation of a different scope")
    url = f"{mds_url}/v3/annotations/{annotation_id}"
    headers = {"case-id": case_id}
    raw_annotation = await http_client.delete(url, headers=headers)
    return raw_annotation


# PRIMITIVES


async def fetch_primitives(
    primitive_query: PrimitiveQuery, skip: int = None, limit: int = None, case_id: str = None
) -> dict:
    url = f"{mds_url}/v3/primitives/query"
    query = primitive_query.model_dump()
    params = {}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    headers = {"case-id": case_id}
    stream_primitives = await http_client.put_stream_response(url, json=query, params=params, headers=headers)
    return stream_primitives


async def fetch_primitives_unique_references(primitive_query: PrimitiveQuery, case_id: str = None) -> dict:
    url = f"{mds_url}/v3/primitives/query/unique-references"
    query = primitive_query.model_dump()
    headers = {"case-id": case_id}
    stream_primitives = await http_client.put_stream_response(url, json=query, headers=headers)
    return stream_primitives


async def post_primitives(primitives: PostPrimitives, case_id: str = None) -> dict:
    primitives_dict = primitives.model_dump()
    url = f"{mds_url}/v3/primitives"
    headers = {"case-id": case_id}
    raw_primitives = await http_client.post(url, json=primitives_dict, headers=headers)
    return raw_primitives


async def fetch_raw_primitive(primitive_id: str, case_id: str = None) -> dict:
    url = f"{mds_url}/v3/primitives/{primitive_id}"
    headers = {"case-id": case_id}
    raw_primitive = await http_client.get(url, headers=headers)
    return raw_primitive


async def delete_primitive(scope_id: Id, primitive_id: str, case_id: str = None) -> dict:
    primitive = await fetch_raw_primitive(primitive_id, case_id=case_id)
    if primitive["creator_id"] != scope_id:
        raise HTTPException(status_code=412, detail="Not allowed to delete primitive of a different scope")
    url = f"{mds_url}/v3/primitives/{primitive_id}"
    headers = {"case-id": case_id}
    raw_primitive = await http_client.delete(url, headers=headers)
    return raw_primitive


# SLIDEMAPS


async def fetch_pixelmaps(
    pixelmap_query: PixelmapQuery, skip: int = None, limit: int = None, case_id: str = None
) -> dict:
    url = f"{mds_url}/v3/pixelmaps/query"
    query = pixelmap_query.model_dump()
    params = {}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    headers = {"case-id": case_id}
    stream_pixelmaps = await http_client.put_stream_response(url, json=query, params=params, headers=headers)
    return stream_pixelmaps


async def fetch_pixelmaps_unique_references(pixelmap_query: PrimitiveQuery, case_id: str = None) -> dict:
    url = f"{mds_url}/v3/pixelmaps/query/unique-references"
    query = pixelmap_query.model_dump()
    headers = {"case-id": case_id}
    stream_pixelmaps = await http_client.put_stream_response(url, json=query, headers=headers)
    return stream_pixelmaps


async def post_pixelmaps(pixelmaps: PostPixelmaps, case_id: str = None) -> dict:
    pixelmaps_dict = pixelmaps.model_dump()
    url = f"{mds_url}/v3/pixelmaps"
    headers = {"case-id": case_id}
    raw_pixelmaps = await http_client.post(url, json=pixelmaps_dict, headers=headers)
    return raw_pixelmaps


async def fetch_raw_pixelmap(pixelmap_id: str, case_id: str = None) -> dict:
    url = f"{mds_url}/v3/pixelmaps/{pixelmap_id}"
    headers = {"case-id": case_id}
    raw_pixelmap = await http_client.get(url, headers=headers)
    return raw_pixelmap


async def delete_pixelmap(scope_id: Id, pixelmap_id: str, case_id: str = None) -> dict:
    pixelmap = await fetch_raw_pixelmap(pixelmap_id, case_id=case_id)
    if pixelmap["creator_id"] != scope_id:
        raise HTTPException(status_code=412, detail="Not allowed to delete pixelmap of a different scope")
    url = f"{mds_url}/v3/pixelmaps/{pixelmap_id}"
    headers = {"case-id": case_id}
    raw_pixelmap = await http_client.delete(url, headers=headers)
    return raw_pixelmap


async def fetch_pixelmap_tile(
    pixelmap_id: str, level: int, tile_x: int, tile_y: int, accept_encoding: Optional[str], case_id: str = None
):
    url = f"{mds_url}/v3/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data"
    headers = {
        "Accept-Encoding": accept_encoding,
        "case-id": case_id,
    }
    tile = await http_client.get_stream_response(url, headers=headers)
    return tile


async def fetch_pixelmap_tiles(
    pixelmap_id: str,
    level: int,
    start_x: int,
    start_y: int,
    end_x: int,
    end_y: int,
    accept_encoding: Optional[str],
    case_id: str = None,
):
    url_base = f"{mds_url}/v3/pixelmaps/{pixelmap_id}"
    url = url_base + f"/level/{level}/position/start/{start_x}/{start_y}/end/{end_x}/{end_y}/data"
    headers = {
        "Accept-Encoding": accept_encoding,
        "case-id": case_id,
    }
    tiles = await http_client.get_stream_response(url, headers=headers)
    return tiles


async def delete_pixelmap_tile(pixelmap_id: str, level: int, tile_x: int, tile_y: int, case_id: str = None):
    url = f"{mds_url}/v3/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data"
    headers = {"case-id": case_id}
    return await http_client.delete_stream_response(url, headers=headers)


async def get_pixelmap_info(pixelmap_id: str, case_id: str = None):
    url = f"{mds_url}/v3/pixelmaps/{pixelmap_id}/info"
    headers = {"case-id": case_id}
    return await http_client.get_stream_response(url, headers=headers)


# CLASSES


async def fetch_classes(
    class_query: ClassQuery,
    with_unique_class_values: bool = False,
    skip: int = None,
    limit: int = None,
    case_id: str = None,
) -> dict:
    url = f"{mds_url}/v3/classes/query"
    query = class_query.model_dump()
    params = {}
    params["with_unique_class_values"] = with_unique_class_values
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    headers = {"case-id": case_id}
    stream_classes = await http_client.put_stream_response(url, json=query, params=params, headers=headers)
    return stream_classes


async def fetch_classes_unique_references(class_query: ClassQuery, case_id: str = None) -> dict:
    url = f"{mds_url}/v3/classes/query/unique-references"
    query = class_query.model_dump()
    headers = {"case-id": case_id}
    stream_classes = await http_client.put_stream_response(url, json=query, headers=headers)
    return stream_classes


async def fetch_classes_namespaces(scope_id: Id) -> dict:
    namespaces = get_global_namespaces()
    raw_ex = await es_connector.fetch_examination_by_scope_id(scope_id)
    raw_ead = await mps_connector.fetch_raw_ead(raw_ex["app_id"])
    ead_class_namespaces = get_ead_namespace(raw_ead)
    namespaces.update(ead_class_namespaces)

    return namespaces


async def post_classes(classes: PostClasses, case_id: str = None) -> dict:
    classes_dict = classes.model_dump()
    url = f"{mds_url}/v3/classes"
    headers = {"case-id": case_id}
    raw_classes = await http_client.post(url, json=classes_dict, headers=headers)
    return raw_classes


async def post_roi_class(scope_id: Id, annotation_id: str, case_id: str = None) -> dict:
    raw_class = {
        "value": ROI_CLASS_VALUE,
        "reference_id": annotation_id,
        "reference_type": "annotation",
        "creator_id": scope_id,
        "creator_type": "scope",
        "type": "class",
    }
    url = f"{mds_url}/v3/classes"
    headers = {"case-id": case_id}
    raw_classes = await http_client.post(url, json=raw_class, headers=headers)
    return raw_classes


async def fetch_raw_class(class_id: str, case_id: str = None) -> dict:
    url = f"{mds_url}/v3/classes/{class_id}"
    headers = {"case-id": case_id}
    raw_class = await http_client.get(url, headers=headers)
    return raw_class


async def delete_class(scope_id: Id, class_id: str, case_id: str = None) -> dict:
    class_obj = await fetch_raw_class(class_id, case_id=case_id)
    if class_obj["creator_id"] != scope_id:
        raise HTTPException(status_code=412, detail="Not allowed to delete class of a different scope")
    url = f"{mds_url}/v3/classes/{class_id}"
    headers = {"case-id": case_id}
    raw_class = await http_client.delete(url, headers=headers)
    return raw_class


# COLLECTIONS


async def fetch_collections(
    collection_query: CollectionQuery, skip: int = None, limit: int = None, case_id: str = None
) -> dict:
    url = f"{mds_url}/v3/collections/query"
    query = collection_query.model_dump()
    params = {}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    headers = {"case-id": case_id}
    stream_collections = await http_client.put_stream_response(url, json=query, params=params, headers=headers)
    return stream_collections


async def fetch_collections_unique_references(collection_query: CollectionQuery, case_id: str = None) -> dict:
    url = f"{mds_url}/v3/collections/query/unique-references"
    query = collection_query.model_dump()
    headers = {"case-id": case_id}
    stream_collections = await http_client.put_stream_response(url, json=query, headers=headers)
    return stream_collections


async def fetch_item_query_list(
    item_query: ItemQuery, collection_id: str, skip: int = None, limit: int = None, case_id: str = None
) -> dict:
    url = f"{mds_url}/v3/collections/{collection_id}/items/query"
    query = item_query.model_dump()
    params = {}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    headers = {"case-id": case_id}
    stream_item_query = await http_client.put_stream_response(url, json=query, params=params, headers=headers)
    return stream_item_query


async def fetch_item_query_list_unique_reference(
    item_query: ItemQuery, collection_id: str, case_id: str = None
) -> dict:
    url = f"{mds_url}/v3/collections/{collection_id}/items/query/unique-references"
    query = item_query.model_dump()
    headers = {"case-id": case_id}
    stream_item_query = await http_client.put_stream_response(url, json=query, headers=headers)
    return stream_item_query


async def post_collection(collection: PostCollection, case_id: str = None) -> dict:
    collection_dict = collection.model_dump()
    url = f"{mds_url}/v3/collections"
    headers = {"case-id": case_id}
    raw_collection = await http_client.post(url, json=collection_dict, headers=headers)
    return raw_collection


async def fetch_raw_collection(
    collection_id: str, shallow: bool = False, with_leaf_ids: bool = False, case_id: str = None
) -> dict:
    url = f"{mds_url}/v3/collections/{collection_id}"
    params = {"shallow": shallow, "with_leaf_ids": with_leaf_ids}
    headers = {"case-id": case_id}
    raw_collection = await http_client.get(url, params=params, headers=headers)
    return raw_collection


async def fetch_raw_collection_items(collection_id: str, case_id: str = None) -> dict:
    """
    TODO:
    This is a temporal fix for collections exceeding the collection item limit in the annotation service
    when validating classes for collections. The validation logic will be moved soon, and therefore this
    code has to be deleted in any case! For the job validation in the daemon, we should consider making
    the settings route of the MDS public (is currently a private route)
    """
    headers = {"case-id": case_id}
    try:
        url = f"{mds_url}/v3/collections/{collection_id}"
        params = {"shallow": False, "with_leaf_ids": False}
        raw_collection = await http_client.get(url, params=params, headers=headers)
        return raw_collection
    except HTTPException as ex:
        if ex.status_code != 413:
            raise ex

        url_items = f"{mds_url}/v3/collections/{collection_id}/items/query"
        result_collection = []

        skip = 0
        limit = 10000  # hardcoded to the default limit of the annotation service
        while True:
            params = {"skip": skip, "limit": limit}
            raw_collection_items = await http_client.put(url_items, params=params, json={}, headers=headers)
            result_collection.extend(raw_collection_items["items"])
            skip += limit
            if skip > int(raw_collection_items["item_count"]):
                break

        return {"item_count": len(result_collection), "items": result_collection}


async def delete_collection(scope_id: Id, collection_id: str, case_id: str = None) -> dict:
    class_obj = await fetch_raw_collection(collection_id, case_id=case_id)
    if class_obj["creator_id"] != scope_id:
        raise HTTPException(status_code=412, detail="Not allowed to delete collection of a different scope")
    url = f"{mds_url}/v3/collections/{collection_id}"
    headers = {"case-id": case_id}
    raw_collection = await http_client.delete(url, headers=headers)
    return raw_collection


async def validate_post_data_recursive(scope_id: Id, data: dict) -> bool:
    if not isinstance(data, IdObject):
        await validate_post_data(scope_id, data)
        if data.type == "collection" and data.items:
            for item in data.items:
                await validate_post_data_recursive(scope_id, item)


async def validate_post_items(scope_id: Id, post_item_data: PostItems) -> bool:
    if isinstance(post_item_data, get_args(PostItemList)):
        for item in post_item_data.items:
            if not isinstance(item, SlideItem):
                await validate_post_data_recursive(scope_id, item)
    else:
        if not isinstance(post_item_data, SlideItem):
            await validate_post_data_recursive(scope_id, post_item_data)


async def post_items(collection_id: str, items: PostItems, case_id: str = None) -> dict:
    items_dict = items.model_dump()
    url = f"{mds_url}/v3/collections/{collection_id}/items"
    headers = {"case-id": case_id}
    raw_items = await http_client.post(url, json=items_dict, headers=headers)
    return raw_items


async def delete_item(scope_id: Id, collection_id: str, item_id: str, case_id: str = None) -> dict:
    class_obj = await fetch_raw_collection(collection_id, case_id=case_id)
    if class_obj["creator_id"] != scope_id:
        raise HTTPException(status_code=412, detail="Not allowed to delete item from a collection of a different scope")
    url = f"{mds_url}/v3/collections/{collection_id}/items/{item_id}"
    headers = {"case-id": case_id}
    raw_item = await http_client.delete(url, headers=headers)
    return raw_item


# LOCK ROUTES


async def lock_annotation(annotation_id, job_id, case_id: str = None):
    url = f"{mds_url}/v3/jobs/{job_id}/lock/annotations/{annotation_id}"
    headers = {"case-id": case_id}
    await http_client.put(url, headers=headers)


async def lock_primitive(primitive_id, job_id, case_id: str = None):
    url = f"{mds_url}/v3/jobs/{job_id}/lock/primitives/{primitive_id}"
    headers = {"case-id": case_id}
    await http_client.put(url, headers=headers)


async def lock_class(class_id, job_id, case_id: str = None):
    url = f"{mds_url}/v3/jobs/{job_id}/lock/classes/{class_id}"
    headers = {"case-id": case_id}
    await http_client.put(url, headers=headers)


async def lock_collection(collection_id, job_id, case_id: str = None):
    url = f"{mds_url}/v3/jobs/{job_id}/lock/collections/{collection_id}"
    headers = {"case-id": case_id}
    await http_client.put(url, headers=headers)


async def lock_pixelmap(pixelmap_id, job_id, case_id: str = None):
    url = f"{mds_url}/v3/jobs/{job_id}/lock/pixelmaps/{pixelmap_id}"
    headers = {"case-id": case_id}
    await http_client.put(url, headers=headers)
