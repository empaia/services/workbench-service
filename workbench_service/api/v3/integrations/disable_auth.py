from fastapi import Depends


def _dummy():
    pass


# Note: Documentation is provided in default.py
class DisableAuth:
    def __init__(self, settings, logger, http_client):
        self.settings = settings
        self.logger = logger
        self.http_client = http_client

    def global_depends(self):
        return Depends(_dummy)

    async def user_examination_hook(self, user_id, examination_id, auth_payload):
        pass

    async def user_post_examination_hook(self, user_id, examination_id, auth_payload):
        pass

    async def user_case_hook(self, user_id, case_id, auth_payload):
        pass

    async def user_cases_filter_hook(self, user_id, auth_payload):
        return None

    async def user_slide_hook(self, user_id, slide_id, auth_payload):
        pass

    async def scope_hook(self, scope_id, scope_auth_payload):
        pass
