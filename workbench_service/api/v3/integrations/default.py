from typing import List, Optional

from fastapi import Depends, HTTPException, status


def _unauthorized():
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Integration not configured",
    )


class Default:
    """
    Authentication class should provide:
    global_depends - method that returns a callback wrapped in Depends (fastapi deps injection)
    user_*_hook - methods that receive output of the above callback together with relevant data and
        should raise HTTPException with status 401 in case user has no access to particular data item
    """

    def __init__(self, settings, logger, http_client):
        self.settings = settings
        self.logger = logger
        self.http_client = http_client

    def global_depends(self):
        """Basic authentication by (callback) dependency injection. Callback should return auth payload if any."""
        return Depends(_unauthorized)

    async def user_examination_hook(self, user_id, examination_id, auth_payload):
        """Examination-level access. Resolves access to this resource.
        @param user_id id of the user accessing the API
        @param examination_id id of the examination object
        @param auth_payload anything that the global_depends function returns
        """

    async def user_post_examination_hook(self, user_id, examination_id, auth_payload):
        """Called after examination creation
        @param user_id id of the user accessing the API
        @param examination_id id of the newly created examination object
        @param auth_payload anything that the global_depends function returns
        """

    async def user_case_hook(self, user_id, case_id, auth_payload):
        """Case-level access. Resolves access to this resource.
        @param user_id id of the user accessing the API
        @param case_id id of the case object
        @param auth_payload anything that the global_depends function returns
        """

    async def user_cases_filter_hook(self, user_id, auth_payload) -> Optional[List[str]]:
        """Retrieve list of case IDs accessible to the user to be used as a filter
        @param user_id id of the user accessing the API
        @param auth_payload anything that the global_depends function returns
        @returns List[str] filter list of IDs that a particular user can view,
            or None if all data can be accessed
        """
        return None

    async def user_slide_hook(self, user_id, slide_id, auth_payload):
        """Slide-level access. Resolves access to this resource.
        @param user_id id of the user accessing the API
        @param slide_id id of the slide object
        @param auth_payload anything that the global_depends function returns
        """

    async def scope_hook(self, scope_id, scope_auth_payload):
        """Scope-level access. Resolves access to this resource.
        @param scope_id id of the scope
        @param scope_auth_payload anything that the scope_validation.scope_depends function returns
        """
