from typing import Dict

from fastapi import Path

from workbench_service.api.v2.connectors import es_connector

from .....models.v1.annotation.annotations import (
    Annotation,
    AnnotationCountResponse,
    AnnotationList,
    AnnotationQuery,
    AnnotationQueryPosition,
    Annotations,
    AnnotationUniqueClassesQuery,
    AnnotationViewerList,
    AnnotationViewerQuery,
    PostAnnotations,
    UniqueClassValues,
)
from .....models.v1.annotation.classes import Class, Classes, ClassList, ClassQuery, PostClasses
from .....models.v1.annotation.collections import (
    Collection,
    CollectionList,
    CollectionQuery,
    ItemPostResponse,
    ItemQuery,
    ItemQueryList,
    PostCollection,
    PostItems,
)
from .....models.v1.annotation.primitives import PostPrimitives, Primitive, PrimitiveList, PrimitiveQuery, Primitives
from .....models.v1.commons import Id, IdObject, UniqueReferences
from ... import params
from ...connectors import annot_connector
from ...custom_models.annotations import ClassesDict
from ...singletons import scope_validation


def add_routes(app, late_init):
    # ANNOTATIONS

    @app.put(
        "/{scope_id}/annotations/query",
        tags=["data panel"],
        summary="Query annotations",
        status_code=200,
        responses={200: {"model": AnnotationList}},
    )
    async def _(
        query: AnnotationQuery,
        with_classes: bool = False,
        with_low_npp_centroids: bool = False,
        skip: int = None,
        limit: int = None,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await annot_connector.validate_query(scope_id, query)
        return await annot_connector.fetch_annotations(
            annotation_query=query,
            with_classes=with_classes,
            with_low_npp_centroids=with_low_npp_centroids,
            skip=skip,
            limit=limit,
        )

    @app.put(
        "/{scope_id}/annotations/query/count",
        tags=["data panel"],
        summary="Get count of queried annotation items",
        status_code=200,
        responses={200: {"model": AnnotationCountResponse}},
    )
    async def _(
        query: AnnotationQuery,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await annot_connector.validate_query(scope_id, query)
        return await annot_connector.fetch_annotations_route_suffix(query.model_dump(), "count")

    @app.put(
        "/{scope_id}/annotations/query/unique-class-values",
        tags=["data panel"],
        summary="Get unique class values of queried annotations",
        status_code=200,
        responses={200: {"model": UniqueClassValues}},
    )
    async def _(
        query: AnnotationUniqueClassesQuery,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await annot_connector.validate_query(scope_id, query)
        return await annot_connector.fetch_annotations_route_suffix(query.model_dump(), "unique-class-values")

    @app.put(
        "/{scope_id}/annotations/query/unique-references",
        tags=["data panel"],
        summary="Get unique references of queried annotations",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(
        query: AnnotationQuery,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await annot_connector.validate_query(scope_id, query)
        return await annot_connector.fetch_annotations_route_suffix(query.model_dump(), "unique-references")

    @app.put(
        "/{scope_id}/annotations/query/viewer",
        tags=["data panel"],
        summary="Query annotations for viewing purposes",
        status_code=200,
        responses={200: {"model": AnnotationViewerList}},
    )
    async def _(
        query: AnnotationViewerQuery,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await annot_connector.validate_query(scope_id, query)
        return await annot_connector.fetch_annotations_route_suffix(query.model_dump(), "viewer")

    @app.put(
        "/{scope_id}/annotations/{annotation_id}/query",
        tags=["data panel"],
        summary="Get the postion of an annotation in the result query",
        status_code=200,
        responses={200: {"model": AnnotationQueryPosition}},
    )
    async def _(
        query: AnnotationQuery,
        annotation_id: str = params.annotation_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await annot_connector.validate_query(scope_id, query)
        return await annot_connector.fetch_annotation_query_position(query, annotation_id)

    @app.post(
        "/{scope_id}/annotations",
        tags=["data panel"],
        summary="Post annotations",
        status_code=200,
        responses={200: {"model": Annotations}},
    )
    async def _(
        annotations: PostAnnotations,
        is_roi: bool = False,
        external_ids: bool = False,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        await annot_connector.validate_post_data(scope_id, annotations)
        raw_annotation = await annot_connector.post_annotations(annotations, external_ids)
        if is_roi:
            await annot_connector.post_roi_class(scope_id, raw_annotation["id"])
        return await annot_connector.fetch_raw_annotation(raw_annotation["id"], with_classes=True)

    @app.get(
        "/{scope_id}/annotations/{annotation_id}",
        tags=["data panel"],
        summary="Get annotation by ID",
        status_code=200,
        responses={200: {"model": Annotation}},
    )
    async def _(
        annotation_id: str = params.annotation_id,
        with_classes: bool = False,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        return await annot_connector.fetch_raw_annotation(annotation_id, with_classes)

    @app.delete(
        "/{scope_id}/annotations/{annotation_id}",
        tags=["data panel"],
        summary="Delete annotation by ID",
        status_code=200,
        responses={200: {"model": IdObject}},
    )
    async def _(
        annotation_id: str = params.annotation_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        return await annot_connector.delete_annotation(scope_id, annotation_id)

    # CLASSES

    @app.put(
        "/{scope_id}/classes/query",
        tags=["data panel"],
        summary="Query classes",
        status_code=200,
        responses={200: {"model": ClassList}},
    )
    async def _(
        query: ClassQuery,
        with_unique_class_values: bool = False,
        skip: int = None,
        limit: int = None,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await annot_connector.validate_query(scope_id, query)
        return await annot_connector.fetch_classes(
            class_query=query, with_unique_class_values=with_unique_class_values, skip=skip, limit=limit
        )

    @app.put(
        "/{scope_id}/classes/query/unique-references",
        tags=["data panel"],
        summary="Get unique references of queried classes",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(
        query: ClassQuery,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await annot_connector.validate_query(scope_id, query)
        return await annot_connector.fetch_classes_unique_references(query)

    @app.post(
        "/{scope_id}/classes",
        tags=["data panel"],
        summary="Post classes",
        status_code=200,
        responses={200: {"model": Classes}},
    )
    async def _(
        classes: PostClasses,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        await annot_connector.validate_post_data(scope_id, classes)
        await annot_connector.validate_class_namespace(scope_id, classes)
        return await annot_connector.post_classes(classes)

    @app.get(
        "/{scope_id}/classes/{class_id}",
        tags=["data panel"],
        summary="Get class by ID",
        status_code=200,
        responses={200: {"model": Class}},
    )
    async def _(
        class_id: str = params.class_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        return await annot_connector.fetch_raw_class(class_id)

    @app.get(
        "/{scope_id}/class-namespaces",
        tags=["data panel"],
        summary="Get class namespaces",
        status_code=200,
        responses={200: {"model": Dict[str, ClassesDict]}},
    )
    async def _(
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        return await annot_connector.fetch_classes_namespaces(scope_id)

    @app.delete(
        "/{scope_id}/classes/{class_id}",
        tags=["data panel"],
        summary="Delete class by ID",
        status_code=200,
        responses={200: {"model": IdObject}},
    )
    async def _(
        class_id: str = params.class_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        return await annot_connector.delete_class(scope_id, class_id)

    # COLLECTIONS

    @app.put(
        "/{scope_id}/collections/query",
        tags=["data panel"],
        summary="Query collections",
        status_code=200,
        responses={200: {"model": CollectionList}},
    )
    async def _(
        query: CollectionQuery,
        skip: int = None,
        limit: int = None,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await annot_connector.validate_query(scope_id, query)
        return await annot_connector.fetch_collections(collection_query=query, skip=skip, limit=limit)

    @app.put(
        "/{scope_id}/collections/query/unique-references",
        tags=["data panel"],
        summary="Get unique references of queried collections",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(
        query: CollectionQuery,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await annot_connector.validate_query(scope_id, query)
        return await annot_connector.fetch_collections_unique_references(query)

    @app.put(
        "/{scope_id}/collections/{collection_id}/items/query",
        tags=["data panel"],
        summary="Query items of collection",
        status_code=200,
        responses={200: {"model": ItemQueryList}},
    )
    async def _(
        query: ItemQuery,
        collection_id: str = params.collection_id,
        skip: int = None,
        limit: int = None,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        return await annot_connector.fetch_item_query_list(
            item_query=query, collection_id=collection_id, skip=skip, limit=limit
        )

    @app.put(
        "/{scope_id}/collections/{collection_id}/items/query/unique-references",
        tags=["data panel"],
        summary="Get unique references of queried items of a collection",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(
        query: ItemQuery,
        collection_id: str = params.collection_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        return await annot_connector.fetch_item_query_list_unique_reference(
            item_query=query, collection_id=collection_id
        )

    @app.post(
        "/{scope_id}/collections",
        tags=["data panel"],
        summary="Post collection",
        status_code=200,
        responses={200: {"model": Collection}},
    )
    async def _(
        collection: PostCollection,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        await annot_connector.validate_post_data_recursive(scope_id, collection)
        return await annot_connector.post_collection(collection)

    @app.get(
        "/{scope_id}/collections/{collection_id}",
        tags=["data panel"],
        summary="Get collection by ID",
        status_code=200,
        responses={200: {"model": Collection}},
    )
    async def _(
        collection_id: str = params.collection_id,
        shallow: bool = False,
        with_leaf_ids: bool = False,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        return await annot_connector.fetch_raw_collection(collection_id, shallow=shallow, with_leaf_ids=with_leaf_ids)

    @app.delete(
        "/{scope_id}/collections/{collection_id}",
        tags=["data panel"],
        summary="Delete collection by ID",
        status_code=200,
        responses={200: {"model": IdObject}},
    )
    async def _(
        collection_id: str = params.collection_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        return await annot_connector.delete_collection(scope_id, collection_id)

    @app.post(
        "/{scope_id}/collections/{collection_id}/items",
        tags=["data panel"],
        summary="Get collection item to existing collection",
        status_code=200,
        responses={200: {"model": ItemPostResponse}},
    )
    async def _(
        items: PostItems,
        collection_id: str = params.collection_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        await annot_connector.validate_post_items(scope_id, items)
        return await annot_connector.post_items(collection_id, items)

    @app.delete(
        "/{scope_id}/collections/{collection_id}/items/{item_id}",
        tags=["data panel"],
        summary="Delete collection item from existing collection",
        status_code=200,
        responses={200: {"model": IdObject}},
    )
    async def _(
        collection_id: str = params.collection_id,
        item_id: str = params.item_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        return await annot_connector.delete_item(scope_id, collection_id, item_id)

    # PRIMITIVES

    @app.put(
        "/{scope_id}/primitives/query",
        tags=["data panel"],
        summary="Query primitives",
        status_code=200,
        responses={200: {"model": PrimitiveList}},
    )
    async def _(
        query: PrimitiveQuery,
        skip: int = None,
        limit: int = None,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await annot_connector.validate_query(scope_id, query)
        return await annot_connector.fetch_primitives(primitive_query=query, skip=skip, limit=limit)

    @app.put(
        "/{scope_id}/primitives/query/unique-references",
        tags=["data panel"],
        summary="Get unique references of queried primitives",
        status_code=200,
        responses={200: {"model": UniqueReferences}},
    )
    async def _(
        query: PrimitiveQuery,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await annot_connector.validate_query(scope_id, query)
        return await annot_connector.fetch_primitives_unique_references(query)

    @app.post(
        "/{scope_id}/primitives",
        tags=["data panel"],
        summary="Post primitives",
        status_code=200,
        responses={200: {"model": Primitives}},
    )
    async def _(
        primitives: PostPrimitives,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        await annot_connector.validate_post_data(scope_id, primitives)
        return await annot_connector.post_primitives(primitives)

    @app.get(
        "/{scope_id}/primitives/{primitive_id}",
        tags=["data panel"],
        summary="Get primitive by ID",
        status_code=200,
        responses={200: {"model": Primitive}},
    )
    async def _(
        primitive_id: str = params.primitive_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        return await annot_connector.fetch_raw_primitive(primitive_id)

    @app.delete(
        "/{scope_id}/primitives/{primitive_id}",
        tags=["data panel"],
        summary="Delete primitive by ID",
        status_code=200,
        responses={200: {"model": IdObject}},
    )
    async def _(
        primitive_id: str = params.primitive_id,
        scope_id: Id = Path(...),
        payload=scope_validation.scope_depends(),
    ):
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        return await annot_connector.delete_primitive(scope_id, primitive_id)
