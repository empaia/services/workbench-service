from . import base, data, jobs, slides


def add_routes(app, late_init):
    base.add_routes(app, late_init)
    jobs.add_routes(app, late_init)
    slides.add_routes(app, late_init)
    data.add_routes(app, late_init)
