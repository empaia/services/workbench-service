from .....models.v1.commons import Id, Message
from ...connectors import es_connector, mps_connector
from ...custom_models.examinations import ExtendedScope
from ...singletons import scope_validation


def add_routes(app, late_init):
    @app.get(
        "/{scope_id}",
        tags=["scope panel"],
        summary="Get scope metadata",
        responses={
            200: {"model": ExtendedScope},
            404: {
                "model": Message,
                "description": "The scope was not found",
            },
        },
    )
    async def _(
        scope_id: Id,
        payload=scope_validation.scope_depends(),
    ):
        scope_data = await es_connector.fetch_raw_scope_data(scope_id)

        raw_examination = await es_connector.fetch_raw_examination(scope_data["examination_id"])
        raw_ead = await mps_connector.fetch_raw_ead(scope_data["app_id"])
        scope_data["case_id"] = raw_examination["case_id"]
        scope_data["examination_state"] = raw_examination["state"]
        scope_data["ead"] = raw_ead

        return scope_data
