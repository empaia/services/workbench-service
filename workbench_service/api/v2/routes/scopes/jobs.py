from fastapi import Body, HTTPException, Path, status

from .....models.v1.commons import Id, IdObject
from .....models.v1.job import Job, JobList, JobStatus, PostJob
from .....singletons import http_client, mds_url
from ...connectors import annot_connector, cds_connector, es_connector, idms_connector, jes_connector, js_connector
from ...custom_models.id_mapper import MappingType
from ...custom_models.jobs import JOB_FINISHED_STATES
from ...custom_models.slides import JobSlideList
from ...singletons import scope_validation

ANNOTATION_TYPES = ["point", "line", "arrow", "rectangle", "circle", "polygon"]
PRIMITIVE_TYPES = ["integer", "float", "string", "bool"]


def add_routes(app, late_init):
    @app.post(
        "/{scope_id}/jobs",
        tags=["jobs panel"],
        summary="Create job",
        response_model=Job,
        responses={
            200: {"model": Job, "description": "A new job ready for assembly"},
            400: {"description": "App/EAD not found"},
            412: {"description": "If the job's creator_id does not match the scope_id"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope to create the job for"),
        job_data: PostJob = Body(..., description="Job creation attributes, creator_id needs to match scope_id"),
        payload=scope_validation.scope_depends(),
    ):
        if scope_id != job_data.creator_id:
            raise HTTPException(
                status_code=status.HTTP_412_PRECONDITION_FAILED,
                detail="The job must contain the given scope_id as creator_id",
            )
        if job_data.creator_type != "SCOPE":
            raise HTTPException(
                status_code=status.HTTP_412_PRECONDITION_FAILED, detail="The job must have SCOPE set as creator_type"
            )
        scope_data = await es_connector.fetch_raw_scope_data(scope_id=scope_id)
        raw_job = await js_connector.create_job_raw(job_data.model_dump())
        await es_connector.put_examination_job(scope_data["examination_id"], job_data.app_id, raw_job["id"])
        return raw_job

    @app.get(
        "/{scope_id}/jobs",
        tags=["jobs panel"],
        summary="Get jobs",
        response_model=JobList,
        responses={
            200: {"model": JobList, "description": "The list of jobs belonging to the given scope"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope to get jobs for"),
        payload=scope_validation.scope_depends(),
    ):
        return await js_connector.fetch_raw_jobs_by_creator(scope_id)

    @app.get(
        "/{scope_id}/jobs-out-of-scope",
        tags=["jobs panel"],
        summary="Get jobs out of scope",
        response_model=JobList,
        responses={
            200: {
                "model": JobList,
                "description": "The list of jobs not in current scope (but of same app and in same examination)",
            },
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope to get jobs for"),
        payload=scope_validation.scope_depends(),
    ):
        return await fetch_jobs_out_of_scope(scope_id)

    @app.put(
        "/{scope_id}/jobs/{job_id}/inputs/{input_key}",
        tags=["jobs panel"],
        summary="Update input of job",
        response_model=Job,
        responses={
            200: {"model": Job, "description": "The updated job"},
            400: {"description": "Job was not found, input_key is not valid or input resource not valid"},
            412: {"description": "The job does not belong to the given scope"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job that should be updated"),
        input_key: str = Path(..., description="Identifier for the input to update"),
        input_id_obj: IdObject = Body(..., description="An object containing the ID of the given input"),
        payload=scope_validation.scope_depends(),
    ):
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        job = await get_scope_validated_job(job_id, scope_id, with_ead=True)
        validate_input_key(input_key, job)
        return await js_connector.update_job_input_raw(job["id"], input_key, input_id_obj.id)

    @app.delete(
        "/{scope_id}/jobs/{job_id}/inputs/{input_key}",
        tags=["jobs panel"],
        summary="Remove input from job",
        response_model=Job,
        responses={
            200: {"model": Job, "description": "The updated job"},
            400: {"description": "Job was not found or input_key is not valid"},
            412: {"description": "The job does not belong to the given scope"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job that should be updated"),
        input_key: str = Path(..., description="Identifier for the input to remove"),
        payload=scope_validation.scope_depends(),
    ):
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        job = await get_scope_validated_job(job_id, scope_id)
        return await js_connector.delete_job_input_raw(job["id"], input_key)

    @app.delete(
        "/{scope_id}/jobs/{job_id}",
        tags=["jobs panel"],
        summary="Remove job",
        response_model=Job,
        responses={
            200: {"model": Job, "description": "The deleted job"},
            400: {"description": "Job not found or not in assembly state"},
            412: {"description": "The job does not belong to the given scope"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job that should be updated"),
        payload=scope_validation.scope_depends(),
    ):
        raw_examination = await es_connector.validate_examination_state_open(scope_id=scope_id)
        job = await get_scope_validated_job(job_id=job_id, scope_id=scope_id)
        await js_connector.delete_job_raw(job_id=job["id"])
        await es_connector.delete_examination_job(ex_id=raw_examination["id"], job_id=job["id"])
        return job

    @app.put(
        "/{scope_id}/jobs/{job_id}/run",
        tags=["jobs panel"],
        summary="Run job",
        response_model=Job,
        responses={
            200: {"model": Job, "description": "The scheduled or failed job"},
            400: {"description": "Job was not found or has open or invalid inputs"},
            412: {"description": "The job does not belong to the given scope"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job that should be executed"),
        payload=scope_validation.scope_depends(),
    ):
        await es_connector.validate_examination_state_open(scope_id=scope_id)
        job = await get_scope_validated_job(job_id, scope_id, with_ead=True)
        await validate_inputs(job)
        class_ids = await validate_classes_of_inputs(job, scope_id)
        await lock_inputs(job, class_ids)
        result = await execute_job(job)
        late_init.daemon_tick()
        return result

    @app.put(
        "/{scope_id}/jobs/{job_id}/stop",
        tags=["jobs panel"],
        summary="Stop job",
        status_code=200,
        responses={
            200: {"model": bool},
            400: {"description": "Malformed Job ID"},
            404: {"description": "Job Execution not found"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job that should be executed"),
        payload=scope_validation.scope_depends(),
    ):
        job = await get_scope_validated_job(job_id, scope_id, with_ead=True)
        return await jes_connector.stop_job_execution(job["id"])

    @app.get(
        "/{scope_id}/jobs/{job_id}/slides",
        tags=["jobs panel"],
        summary="Get slides used in a given job",
        response_model=JobSlideList,
        responses={
            200: {"model": JobSlideList, "description": "The list of slides used in the given job"},
            400: {"description": "Job was not found"},
            412: {"description": "The job does not belong to the given scope"},
        },
    )
    async def _(
        scope_id: Id = Path(..., description="Id of scope the given job belongs to"),
        job_id: Id = Path(..., description="Id of job that should be executed"),
        payload=scope_validation.scope_depends(),
    ):
        job = await get_scope_validated_job(job_id, scope_id, with_ead=True)
        tags = await cds_connector.fetch_raw_tags()
        slide_ids = await get_slide_ids_for_job(job)
        slides = []
        for slide_id in slide_ids:
            slide = await cds_connector.fetch_raw_slide(slide_id)
            slide["mds_url"], slide["local_id"] = await idms_connector.fetch_mapping(slide_id, MappingType.SLIDES)
            slide["stain"], slide["tissue"] = await cds_connector.get_raw_stain_and_tissue_mapping(slide, tags)
            slides.append(slide)
        return JobSlideList(items=slides, item_count=len(slides))


async def get_scope_validated_job(job_id, scope_id, with_ead=False):
    job = await js_connector.fetch_raw_job(job_id, with_ead=with_ead)
    if job["creator_id"] != scope_id:
        raise HTTPException(
            status_code=status.HTTP_412_PRECONDITION_FAILED, detail="The job does not belong to the given scope"
        )
    return job


def validate_input_key(input_key, job):
    ead_input = job["ead"]["inputs"].get(input_key)
    if not ead_input:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="The given input_key is invalid")


async def validate_classes_of_inputs(job, scope_id):
    valid_ead_classes = []
    if "classes" in job["ead"]:
        valid_ead_classes = get_ead_classes_recursive(f"{job['ead']['namespace']}", "classes", job["ead"]["classes"])
    classes_to_lock = []
    for key, value in job["ead"]["inputs"].items():
        temp_classes = await validate_ead_input_classes(value, job["inputs"][key], scope_id, valid_ead_classes)
        if temp_classes:
            classes_to_lock.extend(temp_classes)
    return classes_to_lock


def get_ead_classes_recursive(base_name: str, class_key, class_node):
    base_name = f"{base_name}.{class_key}"
    class_values = []
    if "name" in class_node:
        class_values.append(base_name)
    else:
        for key, value in class_node.items():
            class_values.extend(get_ead_classes_recursive(base_name, key, value))
    return class_values


async def validate_ead_input_classes(partial_input_ead, input_id, scope_id, ead_classes):
    class_ids = []
    if "classes" in partial_input_ead:
        annotation = await annot_connector.fetch_raw_annotation(input_id, with_classes=True)
        if len(annotation["classes"]) == 0:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail="Class constraint for annotation not fullfilled"
            )
        valid_classes = get_valid_class_values_for_constraint(ead_classes, partial_input_ead["classes"])
        for annot_class in annotation["classes"]:
            if annot_class["value"] in valid_classes and annot_class["creator_id"] == scope_id:
                class_ids.append(annot_class["id"])
        if len(class_ids) == 0:
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST, detail="Class constraint for annotation not fullfilled"
            )
    if partial_input_ead["type"] == "collection":
        collection = await annot_connector.fetch_raw_collection(input_id)
        for item in collection["items"]:
            ids = await validate_ead_input_classes(partial_input_ead["items"], item["id"], scope_id, ead_classes)
            class_ids.extend(ids)
    return class_ids


def get_valid_class_values_for_constraint(ead_class_values, class_values_contraints):
    valid_class_values = []
    for value in class_values_contraints:
        resolved_class_values = [cl for cl in ead_class_values if value in cl]
        if resolved_class_values:
            valid_class_values.extend(resolved_class_values)
        else:
            valid_class_values.append(value)
    return valid_class_values


async def validate_inputs(job):
    for input_key in job["ead"]["inputs"]:
        input_id = job["inputs"].get(input_key)
        if not input_id:
            raise HTTPException(status.HTTP_400_BAD_REQUEST, detail=f"Input {input_key} has not been set")
        input_spec = job["ead"]["inputs"][input_key]
        await validate_input(input_id, input_spec)


async def validate_input(input_id, input_spec):
    input_type = input_spec["type"]
    if input_type == "wsi":
        await validate_slide(input_id)
    elif input_type in ANNOTATION_TYPES:
        await validate_annotation(input_id, input_spec)
    elif input_type in PRIMITIVE_TYPES:
        await validate_primitive(input_id, input_spec)
    elif input_type == "class":
        await validate_class(input_id)
    elif input_type == "collection":
        await validate_collection(input_id, input_spec)


async def validate_slide(slide_id):
    try:
        _ = await cds_connector.fetch_raw_slide(slide_id)
    except HTTPException as exc:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=exc.detail) from exc


async def validate_annotation(annotation_id, input_spec):
    try:
        annotation = await annot_connector.fetch_raw_annotation(annotation_id)
    except HTTPException as exc:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=exc.detail) from exc
    if annotation["type"] != input_spec["type"]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Input resource must be an annotation of type {input_spec['type']}",
        )


async def validate_primitive(primitive_id, input_spec):
    try:
        primitive = await annot_connector.fetch_raw_primitive(primitive_id)
    except HTTPException as exc:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=exc.detail) from exc
    if primitive["type"] != input_spec["type"]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Input resource must be a primitive of type {input_spec['type']}",
        )


async def validate_class(class_id):
    try:
        _ = await annot_connector.fetch_raw_class(class_id)
    except HTTPException as exc:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=exc.detail) from exc


async def validate_collection(collection_id, input_spec):
    try:
        collection = await annot_connector.fetch_raw_collection(collection_id, shallow=True)
    except HTTPException as exc:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=exc.detail) from exc
    validate_collection_structure(collection, input_spec)


def validate_collection_structure(collection, collection_spec):
    if collection["item_type"] != collection_spec["items"]["type"]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Input collection specification does not match"
        )
    if collection_spec["items"]["type"] == "collection":
        for item in collection["items"]:
            validate_collection_structure(item, collection_spec["items"])


async def lock_inputs(job, class_ids=None):
    for input_id, input_spec in zip(job["inputs"].values(), job["ead"]["inputs"].values()):
        input_type = input_spec["type"]
        if input_type == "wsi":
            await annot_connector.lock_slide(input_id, job["id"])
        elif input_type in ANNOTATION_TYPES:
            await annot_connector.lock_annotation(input_id, job["id"])
        elif input_type in PRIMITIVE_TYPES:
            await annot_connector.lock_primitive(input_id, job["id"])
        elif input_type == "class":
            await annot_connector.lock_class(input_id, job["id"])
        elif input_type == "collection":
            await annot_connector.lock_collection(input_id, job["id"])
    if class_ids:
        for class_id in class_ids:
            await annot_connector.lock_class(class_id, job["id"])


async def execute_job(job):
    return await js_connector.update_job_status_raw(job["id"], JobStatus.READY)


async def get_slide_ids_for_job(job):
    slide_ids = []
    for input_key, input_id in job["inputs"].items():
        input_spec = job["ead"]["inputs"][input_key]
        if input_spec["type"] == "wsi":
            slide_ids.append(input_id)
        elif is_wsi_collection(input_spec):
            collection = await annot_connector.fetch_raw_collection(input_id, shallow=False)
            slide_ids.extend(flatten_collection_item_ids(collection))
    return slide_ids


def is_wsi_collection(spec):
    while spec["type"] == "collection":
        spec = spec["items"]
    return spec["type"] == "wsi"


def flatten_collection_item_ids(collection):
    items = []
    if collection["item_type"] == "collection":
        for nested in collection["items"]:
            items.extend(flatten_collection_item_ids(nested))
        return items
    return [item["id"] for item in collection["items"]]


async def fetch_jobs_out_of_scope(scope_id):
    raw_scope_data = await es_connector.fetch_raw_scope_data(scope_id)
    ex_id = raw_scope_data["examination_id"]
    app_id = raw_scope_data["app_id"]
    raw_ex = await es_connector.fetch_raw_examination(ex_id)
    jobs = []
    for app in raw_ex["apps"]:
        if app["id"] == app_id:
            jobs = app["jobs"]
            break
    jobs_in_scope = await js_connector.fetch_raw_jobs_by_creator(scope_id)
    job_ids_to_exclude = []
    if len(jobs_in_scope["items"]) > 0:
        job_ids_to_exclude = [job["id"] for job in jobs_in_scope["items"]]
    jobs = list(set(jobs).difference(set(job_ids_to_exclude)))
    if len(jobs) == 0:
        return JobList(item_count=0, items=[])
    url = f"{mds_url}/v1/jobs/query"
    raw_query = {"jobs": jobs, "statuses": JOB_FINISHED_STATES}
    return await http_client.put(url, json=raw_query)
