from fastapi import HTTPException

from workbench_service.api.v2.connectors import mps_connector

from .....singletons import default_generic_app_ui_config
from ...singletons import app_ui_csp_cache, frontend_validation, http_client, settings


def add_routes(app, late_init):
    @app.get(
        "/{frontend_token}/{resource_path:path}", tags=["frontend"], summary="Get resource files of an app frontend"
    )
    async def _(
        resource_path: str,
        payload=frontend_validation.frontend_depends(),
    ):
        app_id = payload["app_id"]
        app_ui_url = payload["app_ui_url"]

        headers = {"organization-id": settings.organization_id}
        response = await http_client.get_stream_response(f"{app_ui_url.rstrip('/')}/{resource_path}", headers=headers)

        if response.status_code != 200:
            raise HTTPException(status_code=404, detail="Failed loading frontend resource.")

        if not settings.disable_csp_settings:
            csp = await _fetch_csp_settings(app_id=app_id)
            response.headers["Content-Security-Policy"] = "; ".join(csp)

        return response

    async def _fetch_csp_settings(app_id: str):
        csp = app_ui_csp_cache.get(app_id)

        if not csp:
            csp_frontend_url = f"{settings.frontend_csp_url}" if settings.frontend_csp_url else "'self'"

            # csp default settings needed for all app uis
            csp_default_src = f"default-src {csp_frontend_url}"
            csp_img_src = f"img-src {csp_frontend_url} blob:"
            csp_worker_src = "worker-src blob:"

            csp = []
            csp_settings = None
            if settings.generic_app_ui_v2_url:
                csp_settings = _assemble_csp_settings(default_generic_app_ui_config["csp"], csp_frontend_url)
            else:
                app_ui_config = await mps_connector.get_app_ui_config(app_id=app_id)
                if app_ui_config and app_ui_config.get("csp"):
                    csp_settings = _assemble_csp_settings(app_ui_config.get("csp"), csp_frontend_url)

            if csp_settings:
                csp.extend(csp_settings)

            csp.extend([csp_default_src, csp_img_src, csp_worker_src])

            if settings.app_ui_connect_src:
                csp.append(f"connect-src {settings.app_ui_connect_src}")

            frame_ancestors = f"frame-ancestors {csp_frontend_url}"
            if settings.app_ui_frame_ancestors:
                frame_ancestors = f"frame-ancestors {settings.app_ui_frame_ancestors}"

            csp.append(frame_ancestors)

            app_ui_csp_cache.add(app_id, csp)

        return csp

    def _assemble_csp_settings(app_ui_csp: dict, csp_frontend_url: str) -> list:
        csp = []
        for key in app_ui_csp:
            csp_setting = app_ui_csp[key]
            if csp_setting:
                new_settings = " ".join([f"'{k}'".replace("_", "-") for k in csp_setting if csp_setting[k]])
                csp_setting_str = f"{key.replace('_', '-')} {csp_frontend_url} " + new_settings
                csp.append(csp_setting_str)
        return csp
