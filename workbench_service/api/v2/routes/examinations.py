from http import HTTPStatus

from fastapi import Header, HTTPException
from fastapi.responses import JSONResponse

from workbench_service.api.v2.connectors import js_connector
from workbench_service.models.marketplace.app import AppUiConfiguration

from ....models.v1.commons import Message
from .. import params
from ..connectors import es_connector, mps_connector
from ..custom_models.apps import AppList, FrontendToken, PostAppResponse
from ..custom_models.examinations import Examination, ExaminationState, ScopeTokenAndScopeID
from ..singletons import access_token_tools, api_integration, settings


def add_routes(app, late_init):
    @app.put(
        "/examinations/{examination_id}/close",
        tags=["examinations panel"],
        status_code=200,
        responses={200: {"model": Examination}},
    )
    async def _(
        examination_id: str = params.ex_id,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_examination_hook(
            user_id=user_id, examination_id=examination_id, auth_payload=payload
        )
        return await es_connector.put_examination_state(examination_id=examination_id, state=ExaminationState.CLOSED)

    @app.get(
        "/examinations/{examination_id}/apps",
        tags=["examinations panel"],
        status_code=200,
        responses={200: {"model": AppList}},
    )
    async def _(
        examination_id: str = params.ex_id,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        return await es_connector.fetch_examination_apps(examination_id=examination_id)

    @app.get(
        "/examinations/{examination_id}/apps/{app_id}/frontend-token",
        tags=["examinations panel"],
        status_code=200,
        responses={200: {"model": FrontendToken}},
    )
    async def _(
        examination_id: str = params.ex_id,
        app_id: str = params.app_id,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_examination_hook(
            user_id=user_id, examination_id=examination_id, auth_payload=payload
        )
        await es_connector.check_app_in_examination(examination_id=examination_id, app_id=app_id)

        app_ui_url = await mps_connector.get_app_ui_url(app_id)
        payload = {"app_id": app_id, "app_ui_url": app_ui_url}

        access_token = access_token_tools.create_token_from_dict(
            payload=payload, expires_after_seconds=settings.frontend_token_exp
        )
        return FrontendToken(access_token=access_token)

    @app.get(
        "/examinations/{examination_id}/apps/{app_id}/app-ui-config",
        tags=["examinations panel"],
        status_code=200,
        responses={200: {"model": AppUiConfiguration}},
    )
    async def _(
        examination_id: str = params.ex_id,
        app_id: str = params.app_id,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_examination_hook(
            user_id=user_id, examination_id=examination_id, auth_payload=payload
        )
        await es_connector.check_app_in_examination(examination_id=examination_id, app_id=app_id)
        app_ui_config = await mps_connector.get_app_ui_config(app_id=app_id)

        if app_ui_config is None:
            raise HTTPException(status_code=HTTPStatus.NOT_FOUND, detail="No app UI configuration for app.")

        return app_ui_config

    @app.put(
        "/examinations/{examination_id}/apps/{app_id}",
        tags=["examinations panel"],
        status_code=200,
        responses={200: {"model": PostAppResponse}},
    )
    async def _(
        examination_id: str = params.ex_id,
        app_id: str = params.app_id,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_examination_hook(
            user_id=user_id, examination_id=examination_id, auth_payload=payload
        )
        await mps_connector.fetch_app_view(app_id)  # check app exists
        return await es_connector.put_examination_app(examination_id=examination_id, app_id=app_id)

    @app.put(
        "/examinations/{examination_id}/apps/{app_id}/scope",
        tags=["examinations panel"],
        responses={
            200: {"model": ScopeTokenAndScopeID},
            201: {"model": ScopeTokenAndScopeID},
            400: {
                "model": Message,
                "description": "The examination was not found or the app was not added to the examination",
            },
        },
    )
    async def _(
        examination_id: str = params.ex_id,
        app_id: str = params.app_id,
        user_id: str = Header(...),
        payload=api_integration.global_depends(),
    ):
        await api_integration.user_examination_hook(
            user_id=user_id, examination_id=examination_id, auth_payload=payload
        )
        status_code, raw_scope = await es_connector.fetch_or_create_raw_scope(
            examination_id=examination_id, app_id=app_id, user_id=user_id
        )
        raw_scope_token = await es_connector.fetch_raw_scope_token(raw_scope["id"])
        await js_connector.post_ead(app_id)
        return JSONResponse(
            status_code=status_code,
            content={"scope_id": raw_scope["id"], "access_token": raw_scope_token["access_token"]},
        )
