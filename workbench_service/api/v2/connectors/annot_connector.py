from typing import List
from xmlrpc.client import boolean

from fastapi.exceptions import HTTPException

from ....models.v1.annotation.annotations import AnnotationQuery, PostAnnotations
from ....models.v1.annotation.classes import ClassQuery, PostClasses
from ....models.v1.annotation.collections import (
    CollectionQuery,
    ItemQuery,
    PostCollection,
    PostItemList,
    PostItems,
    SlideItem,
)
from ....models.v1.annotation.primitives import PostPrimitives, PrimitiveQuery
from ....models.v1.commons import DataCreatorType, Id, IdObject
from ....models.v1.job import Job
from ....singletons import http_client, mds_url, v2_ex_job_cache, v2_scope_ex_cache
from ..connectors import es_connector, mps_connector
from ..custom_models.annotations import ROI_CLASS_VALUE
from ..namespace_validation import get_ead_namespace, get_global_namespaces, validate_class_value

# GENERAL


async def validate_class_namespace(scope_id: Id, query: PostClasses) -> bool:
    scope_data = await es_connector.fetch_raw_scope_data(scope_id)
    raw_ead = await mps_connector.fetch_raw_ead(scope_data["app_id"])
    namespaces = get_global_namespaces()
    ead_namespace = get_ead_namespace(raw_ead)
    namespaces.update(ead_namespace)
    valid = validate_class_value(query.value, namespaces)
    if not valid:
        raise HTTPException(status_code=400, detail="Invalid class name for EAD")


async def validate_query(scope_id: Id, query: dict):
    scope_data = v2_scope_ex_cache.get(scope_id)
    if not scope_data:
        scope_data = await es_connector.fetch_raw_scope_data(scope_id)
        v2_scope_ex_cache.add(scope_id, scope_data)

    ex_id = scope_data["examination_id"]

    valid = await validate_creators(scope_id, query.creators)
    if not valid:
        jobs = v2_ex_job_cache.get(ex_id)
        if jobs and await validate_jobs(scope_data, query.jobs, jobs, raise_exception=False):
            valid = True
        else:
            _, jobs = await es_connector.fetch_examination(examination_id=ex_id)
            v2_ex_job_cache.add(ex_id, jobs)
            valid = await validate_jobs(scope_data, query.jobs, jobs)

    if not valid:
        raise HTTPException(
            status_code=400, detail="Either a valid creator_id or job list must be set as query parameter"
        )


async def validate_creators(scope_id: Id, queried_creators: dict) -> bool:
    if queried_creators is None:
        return False
    if set(queried_creators) != set([scope_id]):
        return False
    return True


async def validate_jobs(scope_data: dict, queried_jobs: dict, valid_jobs: List[Job], raise_exception: bool = True):
    if queried_jobs is None:
        return False

    valid_jobs_dict = {str(j.id): j for j in valid_jobs}
    queried_job_ids = set(queried_jobs)

    if not queried_job_ids.issubset(valid_jobs_dict.keys()):
        if raise_exception:
            raise HTTPException(status_code=412, detail="Invalid job list query parameter for current scope")
        else:
            return False

    for q_job in queried_job_ids:
        if str(valid_jobs_dict[q_job].app_id) != scope_data["app_id"]:
            if raise_exception:
                raise HTTPException(
                    status_code=412, detail=f"Job of app [{scope_data['app_id']}] not allowed for current scope"
                )
            else:
                return False
    return True


async def validate_post_data(scope_id: Id, query: dict):
    if query.creator_id is None or query.creator_id != scope_id:
        raise HTTPException(status_code=412, detail="Creator_id must be set to scope_id")
    if query.creator_type is None or query.creator_type != DataCreatorType.SCOPE:
        raise HTTPException(status_code=412, detail="Creator_type must be set to scope")


# ANNOTATIONS


async def fetch_annotations(
    annotation_query: AnnotationQuery,
    with_classes: bool,
    with_low_npp_centroids: bool,
    skip: int = None,
    limit: int = None,
) -> dict:
    url = f"{mds_url}/v1/annotations/query"
    raw_query = annotation_query.model_dump()
    params = {}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    if with_classes:
        params["with_classes"] = with_classes
    if with_low_npp_centroids:
        params["with_low_npp_centroids"] = with_low_npp_centroids
    stream_annotations = await http_client.put_stream_response(url, params=params, json=raw_query)
    return stream_annotations


async def fetch_raw_annotation(annotation_id: str, with_classes: bool = False) -> dict:
    url = f"{mds_url}/v1/annotations/{annotation_id}"
    params = {}
    if with_classes:
        params["with_classes"] = with_classes
    raw_annotation = await http_client.get(url, params=params)
    return raw_annotation


async def fetch_annotations_route_suffix(query: dict, route_suffix: str) -> dict:
    url = f"{mds_url}/v1/annotations/query/{route_suffix}"
    stream_annotations = await http_client.put_stream_response(url, json=query)
    return stream_annotations


async def fetch_annotation_query_position(query: AnnotationQuery, annotation_id: str) -> dict:
    url = f"{mds_url}/v1/annotations/{annotation_id}/query"
    raw_query = query.model_dump()
    stream_annotations = await http_client.put_stream_response(url, json=raw_query)
    return stream_annotations


async def post_annotations(annotations: PostAnnotations, external_ids: boolean = False) -> dict:
    annotations_dict = annotations.model_dump()
    url = f"{mds_url}/v1/annotations"
    params = {}
    if external_ids:
        params["external_ids"] = external_ids
    raw_annotations = await http_client.post(url, json=annotations_dict, params=params)
    return raw_annotations


async def delete_annotation(scope_id: Id, annotation_id: str) -> dict:
    annotration = await fetch_raw_annotation(annotation_id)
    if annotration["creator_id"] != scope_id:
        raise HTTPException(status_code=412, detail="Not allowed to delete annotation of a different scope")
    url = f"{mds_url}/v1/annotations/{annotation_id}"
    raw_annotation = await http_client.delete(url)
    return raw_annotation


# PRIMITIVES


async def fetch_primitives(primitive_query: PrimitiveQuery, skip: int = None, limit: int = None) -> dict:
    url = f"{mds_url}/v1/primitives/query"
    query = primitive_query.model_dump()
    params = {}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    stream_primitives = await http_client.put_stream_response(url, json=query, params=params)
    return stream_primitives


async def fetch_primitives_unique_references(primitive_query: PrimitiveQuery) -> dict:
    url = f"{mds_url}/v1/primitives/query/unique-references"
    query = primitive_query.model_dump()
    stream_primitives = await http_client.put_stream_response(url, json=query)
    return stream_primitives


async def post_primitives(primitives: PostPrimitives) -> dict:
    primitives_dict = primitives.model_dump()
    url = f"{mds_url}/v1/primitives"
    raw_primitives = await http_client.post(url, json=primitives_dict)
    return raw_primitives


async def fetch_raw_primitive(primitive_id: str) -> dict:
    url = f"{mds_url}/v1/primitives/{primitive_id}"
    raw_primitive = await http_client.get(url)
    return raw_primitive


async def delete_primitive(scope_id: Id, primitive_id: str) -> dict:
    primitive = await fetch_raw_primitive(primitive_id)
    if primitive["creator_id"] != scope_id:
        raise HTTPException(status_code=412, detail="Not allowed to delete primitive of a different scope")
    url = f"{mds_url}/v1/primitives/{primitive_id}"
    raw_primitive = await http_client.delete(url)
    return raw_primitive


# CLASSES


async def fetch_classes(
    class_query: ClassQuery, with_unique_class_values: bool = False, skip: int = None, limit: int = None
) -> dict:
    url = f"{mds_url}/v1/classes/query"
    query = class_query.model_dump()
    params = {}
    params["with_unique_class_values"] = with_unique_class_values
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    stream_classes = await http_client.put_stream_response(url, json=query, params=params)
    return stream_classes


async def fetch_classes_unique_references(class_query: ClassQuery) -> dict:
    url = f"{mds_url}/v1/classes/query/unique-references"
    query = class_query.model_dump()
    stream_classes = await http_client.put_stream_response(url, json=query)
    return stream_classes


async def fetch_classes_namespaces(scope_id: Id) -> dict:
    namespaces = get_global_namespaces()

    scope_data = await es_connector.fetch_raw_scope_data(scope_id)
    raw_ead = await mps_connector.fetch_raw_ead(scope_data["app_id"])
    ead_class_namespaces = get_ead_namespace(raw_ead)
    namespaces.update(ead_class_namespaces)

    return namespaces


async def post_classes(classes: PostClasses) -> dict:
    classes_dict = classes.model_dump()
    url = f"{mds_url}/v1/classes"
    raw_classes = await http_client.post(url, json=classes_dict)
    return raw_classes


async def post_roi_class(scope_id: Id, annotation_id: str) -> dict:
    raw_class = {
        "value": ROI_CLASS_VALUE,
        "reference_id": annotation_id,
        "reference_type": "annotation",
        "creator_id": scope_id,
        "creator_type": "scope",
        "type": "class",
    }
    url = f"{mds_url}/v1/classes"
    raw_classes = await http_client.post(url, json=raw_class)
    return raw_classes


async def fetch_raw_class(class_id: str) -> dict:
    url = f"{mds_url}/v1/classes/{class_id}"
    raw_class = await http_client.get(url)
    return raw_class


async def delete_class(scope_id: Id, class_id: str) -> dict:
    class_obj = await fetch_raw_class(class_id)
    if class_obj["creator_id"] != scope_id:
        raise HTTPException(status_code=412, detail="Not allowed to delete class of a different scope")
    url = f"{mds_url}/v1/classes/{class_id}"
    raw_class = await http_client.delete(url)
    return raw_class


# COLLECTIONS


async def fetch_collections(collection_query: CollectionQuery, skip: int = None, limit: int = None) -> dict:
    url = f"{mds_url}/v1/collections/query"
    query = collection_query.model_dump()
    params = {}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    stream_collections = await http_client.put_stream_response(url, json=query, params=params)
    return stream_collections


async def fetch_collections_unique_references(collection_query: CollectionQuery) -> dict:
    url = f"{mds_url}/v1/collections/query/unique-references"
    query = collection_query.model_dump()
    stream_collections = await http_client.put_stream_response(url, json=query)
    return stream_collections


async def fetch_item_query_list(item_query: ItemQuery, collection_id: str, skip: int = None, limit: int = None) -> dict:
    url = f"{mds_url}/v1/collections/{collection_id}/items/query"
    query = item_query.model_dump()
    params = {}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    stream_item_query = await http_client.put_stream_response(url, json=query, params=params)
    return stream_item_query


async def fetch_item_query_list_unique_reference(item_query: ItemQuery, collection_id: str) -> dict:
    url = f"{mds_url}/v1/collections/{collection_id}/items/query/unique-references"
    query = item_query.model_dump()
    stream_item_query = await http_client.put_stream_response(url, json=query)
    return stream_item_query


async def post_collection(collection: PostCollection) -> dict:
    collection_dict = collection.model_dump()
    url = f"{mds_url}/v1/collections"
    raw_collection = await http_client.post(url, json=collection_dict)
    return raw_collection


async def fetch_raw_collection(collection_id: str, shallow: bool = False, with_leaf_ids: bool = False) -> dict:
    url = f"{mds_url}/v1/collections/{collection_id}"
    raw_collection = await http_client.get(url, params={"shallow": shallow, "with_leaf_ids": with_leaf_ids})
    return raw_collection


async def delete_collection(scope_id: Id, collection_id: str) -> dict:
    class_obj = await fetch_raw_collection(collection_id)
    if class_obj["creator_id"] != scope_id:
        raise HTTPException(status_code=412, detail="Not allowed to delete collection of a different scope")
    url = f"{mds_url}/v1/collections/{collection_id}"
    raw_collection = await http_client.delete(url)
    return raw_collection


async def validate_post_data_recursive(scope_id: Id, data: dict) -> bool:
    if not isinstance(data, IdObject):
        await validate_post_data(scope_id, data)
        if data.type == "collection" and data.items:
            for item in data.items:
                await validate_post_data_recursive(scope_id, item)


async def validate_post_items(scope_id: Id, post_item_data: PostItems) -> bool:
    if isinstance(post_item_data, PostItemList):
        for item in post_item_data.items:
            if not isinstance(item, SlideItem):
                await validate_post_data_recursive(scope_id, item)
    else:
        if not isinstance(post_item_data, SlideItem):
            await validate_post_data_recursive(scope_id, post_item_data)


async def post_items(collection_id: str, items: PostItems) -> dict:
    items_dict = items.model_dump()
    url = f"{mds_url}/v1/collections/{collection_id}/items"
    raw_items = await http_client.post(url, json=items_dict)
    return raw_items


async def delete_item(scope_id: Id, collection_id: str, item_id: str) -> dict:
    class_obj = await fetch_raw_collection(collection_id)
    if class_obj["creator_id"] != scope_id:
        raise HTTPException(status_code=412, detail="Not allowed to delete item from a collection of a different scope")
    url = f"{mds_url}/v1/collections/{collection_id}/items/{item_id}"
    raw_item = await http_client.delete(url)
    return raw_item


# LOCK ROUTES


async def lock_slide(slide_id, job_id):
    url = f"{mds_url}/v1/jobs/{job_id}/lock/slides/{slide_id}"
    await http_client.put(url)


async def lock_annotation(annotation_id, job_id):
    url = f"{mds_url}/v1/jobs/{job_id}/lock/annotations/{annotation_id}"
    await http_client.put(url)


async def lock_primitive(primitive_id, job_id):
    url = f"{mds_url}/v1/jobs/{job_id}/lock/primitives/{primitive_id}"
    await http_client.put(url)


async def lock_class(class_id, job_id):
    url = f"{mds_url}/v1/jobs/{job_id}/lock/classes/{class_id}"
    await http_client.put(url)


async def lock_collection(collection_id, job_id):
    url = f"{mds_url}/v1/jobs/{job_id}/lock/collections/{collection_id}"
    await http_client.put(url)
