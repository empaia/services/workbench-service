from typing import List

from fastapi import HTTPException

from workbench_service.api.v2.connectors import mps_connector

from ....models.v1.commons import Id
from ....singletons import http_client, mds_url


async def create_job_raw(job_data):
    url = f"{mds_url}/v1/jobs"
    return await http_client.post(url, json=job_data)


async def fetch_raw_jobs(jobs: List[str], skip: int = None, limit: int = None) -> dict:
    url = f"{mds_url}/v1/jobs/query"
    raw_query = {}
    if jobs:
        raw_query["jobs"] = jobs
    params = {}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    raw_jobs = await http_client.put(url, json=raw_query, params=params)
    return raw_jobs


async def fetch_raw_job(job_id: Id, with_ead=False):
    url = f"{mds_url}/v1/jobs/{job_id}"
    return await http_client.get(url, params={"with_ead": with_ead})


async def fetch_raw_jobs_by_creator(creator_id: Id):
    raw_query = {"creators": [creator_id]}
    url = f"{mds_url}/v1/jobs/query"
    return await http_client.put(url, json=raw_query)


async def update_job_input_raw(job_id, input_key: str, input_id: Id):
    url = f"{mds_url}/v1/jobs/{job_id}/inputs/{input_key}"
    return await http_client.put(url, json={"id": input_id})


async def update_job_status_raw(job_id, status: str):
    url = f"{mds_url}/v1/jobs/{job_id}/status"
    return await http_client.put(url, json={"status": status})


async def delete_job_input_raw(job_id, input_key: str):
    url = f"{mds_url}/v1/jobs/{job_id}/inputs/{input_key}"
    return await http_client.delete(url)


async def delete_job_raw(job_id):
    url = f"{mds_url}/v1/jobs/{job_id}"
    return await http_client.delete(url)


async def post_ead(app_id: str):
    url = f"{mds_url}/v1/apps/{app_id}/ead"
    try:
        await http_client.get(url)
    except HTTPException as e1:
        # This should ideally bve 404
        if e1.status_code == 400:
            try:
                raw_ead = await mps_connector.fetch_raw_ead(app_id)
                url = f"{mds_url}/v1/apps/{app_id}/ead"
                await http_client.put(url, json=raw_ead)
            except HTTPException as e2:
                # Unfortunately JS now returns 400 on several Errors, so the followin 2 lines are sub-optimal:
                # if e2.status_code != 400:
                #     raise
                # Therefore, the following clunky workaround parsing the error message (instead of status):
                if "App already exists" in e2.detail:
                    pass
                else:
                    raise
        else:
            raise
