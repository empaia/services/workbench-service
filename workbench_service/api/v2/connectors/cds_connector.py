from typing import List, Tuple

from ....singletons import http_client, mds_url
from ..custom_models.cases import Case, CaseList
from ..custom_models.id_mapper import MappingType
from ..custom_models.slides import Slide, SlideList
from . import es_connector, idms_connector


async def fetch_raw_tags() -> dict:
    url = f"{mds_url}/private/v1/tags"
    raw_tags = await http_client.get(url)
    return raw_tags


async def aggregate_case(raw_case: dict, raw_tags: dict, mappings: dict) -> Case:
    mds_url_mapping = None
    local_id_mapping = None
    if raw_case["id"] in mappings:
        mds_url_mapping = mappings[raw_case["id"]]["mds_url"] if "mds_url" in mappings[raw_case["id"]] else None
        local_id_mapping = mappings[raw_case["id"]]["local_id"] if "local_id" in mappings[raw_case["id"]] else None

    stain_tags = set([s["stain"] for s in raw_case["slides"]])
    stains = await get_tag_for_tag_group(raw_tags, stain_tags, "STAIN")

    tissue_tags = set([s["tissue"] for s in raw_case["slides"]])
    tissues = await get_tag_for_tag_group(raw_tags, tissue_tags, "TISSUE")

    blocks = set([s["block"] for s in raw_case["slides"] if s["block"] is not None])

    updated_at = raw_case["updated_at"]
    if updated_at is None:
        updated_at = raw_case["created_at"]

    return Case(
        id=raw_case["id"],
        mds_url=mds_url_mapping,
        local_id=local_id_mapping,
        creator_id=raw_case["creator_id"],
        creator_type=raw_case["creator_type"],
        description=raw_case["description"],
        created_at=raw_case["created_at"],
        updated_at=updated_at,
        slides_count=len(raw_case["slides"]),
        tissues=tissues,
        blocks=blocks,
        stains=stains,
        examinations=[],
        deleted=raw_case["deleted"],
    )


async def get_tag_for_tag_group(raw_tags: dict, group_tags: list, group: str) -> dict:
    tags = {}
    if group not in raw_tags:
        return tags
    for tag in group_tags:
        if tag and tag in raw_tags[group]:
            tags[tag] = raw_tags[group][tag]
    return tags


async def get_raw_stain_and_tissue_mapping(slide: Slide, tags: dict) -> Tuple[dict, dict]:
    tag_stain, tag_tissue = slide.get("stain"), slide.get("tissue")
    stain, tissue = None, None
    if tag_stain and tag_stain in tags["STAIN"]:
        stain = {"tag": tag_stain, "mappings": tags["STAIN"][tag_stain]}
    if tag_tissue and tag_tissue in tags["TISSUE"]:
        tissue = {"tag": tag_tissue, "mappings": tags["TISSUE"][tag_tissue]}
    return stain, tissue


async def fetch_raw_slide(slide_id: str) -> dict:
    url = f"{mds_url}/v1/slides/{slide_id}"
    raw_slide = await http_client.get(url)
    return raw_slide


async def fetch_raw_slides(cases: List[str], skip: int = None, limit: int = None) -> dict:
    url = f"{mds_url}/v1/slides/query"
    raw_query = {}
    if cases:
        raw_query["cases"] = cases
    params = {}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    raw_slides = await http_client.put(url, json=raw_query, params=params)
    return raw_slides


async def fetch_raw_case(case_id: str, with_slides: bool = True) -> dict:
    url = f"{mds_url}/v1/cases/{case_id}"
    params = {"with_slides": with_slides}
    raw_case = await http_client.get(url, params=params)
    return raw_case


async def fetch_case(case_id: str, with_slides: bool = True) -> Case:
    raw_case = await fetch_raw_case(case_id, with_slides)
    raw_tags = await fetch_raw_tags()
    mappings = await idms_connector.fetch_mappings([raw_case["id"]], MappingType.CASES)
    case = await aggregate_case(raw_case, raw_tags, mappings)
    raw_examinations = await es_connector.fetch_raw_examinations([raw_case["id"]])
    case.examinations = raw_examinations["items"]

    return case


async def fetch_raw_cases(with_slides: bool = True, skip: int = None, limit: int = None) -> dict:
    url = f"{mds_url}/v1/cases"
    params = {"with_slides": with_slides}
    if skip:
        params["skip"] = skip
    if limit:
        params["limit"] = limit
    raw_cases = await http_client.get(url, params=params)
    return raw_cases


async def fetch_cases(with_slides: bool = True, skip: int = None, limit: int = None) -> CaseList:
    raw_cases = await fetch_raw_cases(with_slides=with_slides, skip=skip, limit=limit)
    raw_tags = await fetch_raw_tags()

    case_ids = [raw_case["id"] for raw_case in raw_cases["items"]]
    mappings = await idms_connector.fetch_mappings(case_ids, MappingType.CASES)

    # fetch all cases
    cases_dict = {}
    for raw_case in raw_cases["items"]:
        case = await aggregate_case(raw_case, raw_tags, mappings)
        cases_dict[raw_case["id"]] = case

    raw_examinations = await es_connector.fetch_raw_examinations(list(cases_dict.keys()))

    for raw_examination in raw_examinations["items"]:
        cases_dict[raw_examination["case_id"]].examinations.append(raw_examination)

    cases_list = list(cases_dict.values())
    cases = CaseList(item_count=raw_cases["item_count"], items=cases_list)
    return cases


async def fetch_slides(case_id: str, skip: int = None, limit: int = None) -> SlideList:
    raw_tags = await fetch_raw_tags()
    raw_slides = await fetch_raw_slides(cases=[case_id], skip=skip, limit=limit)
    slide_ids = [raw_slide["id"] for raw_slide in raw_slides["items"]]
    mappings = await idms_connector.fetch_mappings(slide_ids, MappingType.SLIDES)

    items = []
    for raw_slide in raw_slides["items"]:
        mds_url_mapping = None
        local_id_mapping = None
        if raw_slide["id"] in mappings:
            mds_url_mapping = mappings[raw_slide["id"]]["mds_url"] if "mds_url" in mappings[raw_slide["id"]] else None
            local_id_mapping = (
                mappings[raw_slide["id"]]["local_id"] if "local_id" in mappings[raw_slide["id"]] else None
            )

        created_at = raw_slide["created_at"]
        updated_at = raw_slide.get("updated_at")

        if updated_at is None:
            updated_at = created_at

        stain, tissue = await get_raw_stain_and_tissue_mapping(raw_slide, raw_tags)

        items.append(
            Slide(
                id=raw_slide["id"],
                mds_url=mds_url_mapping,
                local_id=local_id_mapping,
                block=raw_slide.get("block"),
                stain=stain,
                tissue=tissue,
                case_id=raw_slide["case_id"],
                created_at=created_at,
                updated_at=updated_at,
                deleted=raw_slide["deleted"],
            )
        )

    return SlideList(item_count=raw_slides["item_count"], items=items)
