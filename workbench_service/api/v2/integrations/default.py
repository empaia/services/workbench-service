from fastapi import Depends, HTTPException, status


def _unauthorized():
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Integration not configured",
    )


class Default:
    def __init__(self, settings, logger, http_client):
        self.settings = settings
        self.logger = logger
        self.http_client = http_client

    def global_depends(self):
        return Depends(_unauthorized)

    async def user_examination_hook(self, user_id, examination_id, auth_payload):
        """Examination-level access. Resolves access to this resource.
        @param user_id id of the user accessing the API
        @param examination_id id of the examination object
        @param auth_payload anything that the dependency callback returns
        """

    async def user_post_examination_hook(self, user_id, examination_id, auth_payload):
        """Called after examination creation
        @param user_id id of the user accessing the API
        @param examination_id id of the newly created examination object
        @param auth_payload anything that the dependency callback returns
        """

    async def user_case_hook(self, user_id, case_id, auth_payload):
        """Examination-level access. Resolves access to this resource.
        @param user_id id of the user accessing the API
        @param case_id id of the case object
        @param auth_payload anything that the dependency callback returns
        """

    async def user_slide_hook(self, user_id, slide_id, auth_payload):
        """Slide-level access. Resolves access to this resource.
        @param user_id id of the user accessing the API
        @param slide_id id of the slide object
        @param auth_payload anything that the dependency callback returns
        """
