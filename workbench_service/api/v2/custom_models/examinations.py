from typing import List

from pydantic import Field

from ....models.v1.commons import Id, ItemCount, RestrictedBaseModel, Timestamp
from ....models.v1.examination import ExaminationCreatorType, ExaminationState, Scope, ScopeToken


class Examination(RestrictedBaseModel):
    id: Id = Field(example="b10648a7-340d-43fc-a2d9-4d91cc86f33f", description="ID")
    case_id: Id = Field(example="b10648a7-340d-43fc-a2d9-4d91cc86f33f", description="Case ID")
    creator_id: Id = Field(example="b10648a7-340d-43fc-a2d9-4d91cc86f33f", description="Creator ID")
    creator_type: ExaminationCreatorType = Field(example=ExaminationCreatorType.USER, description="Creator Type")
    state: ExaminationState = Field(example=ExaminationState.OPEN, description="Examination state")
    created_at: Timestamp = Field(
        example=1598611645000, description="Timestamp (milliseconds) when the examination was created"
    )
    updated_at: Timestamp = Field(
        example=1598611645000, description="Timestamp (milliseconds) when the examination was last updated"
    )
    jobs: List[str] = Field(
        example=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="List of job IDs in examination"
    )
    jobs_count: ItemCount = Field(example=10, description="Number of jobs (including finished) in examination")
    jobs_count_finished: ItemCount = Field(example=8, description="Number of finished jobs in examination")
    apps: List[Id] = Field(
        example=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"], description="List of app IDs in examination"
    )


class ExaminationList(RestrictedBaseModel):
    items: List[Examination] = Field(description="List of examinations (affected by skip/limit pagination) in case")
    item_count: ItemCount = Field(
        example=10, description="Number of examinations (not affected by skip/limit pagination) in case"
    )


class ScopeTokenAndScopeID(ScopeToken):
    scope_id: Id = Field(
        example="a10648a7-340d-43fc-a2d9-4d91cc86f33f",
        description="The ID of the scope corresponding to the token",
    )


class ExtendedScope(Scope):
    case_id: Id = Field(example="b10648a7-340d-43fc-a2d9-4d91cc86f33f", description="Case ID")
    examination_state: ExaminationState = Field(
        example=ExaminationState.OPEN, description="State of the scopes examination"
    )
    ead: dict = Field(description="EMPAIA app description")
