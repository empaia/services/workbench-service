from typing import List, Optional

from pydantic import Field

from ....models.v1.commons import Id, ItemCount, RestrictedBaseModel


class App(RestrictedBaseModel):
    id: Id = Field(description="ID")
    app_version: str = Field(example="v2", description="App version")
    name_short: str = Field(example="TAPP01", description="App short name taken from the EAD")
    vendor_name: str = Field(example="SomeVendor", description="Name of the app vendor")
    store_description: Optional[str] = Field(
        example="This app counts tumor cells", description="App description taken from store"
    )
    store_icon_url: Optional[str] = Field(description="URL to app image icon")
    store_url: Optional[str] = Field(description="URL to app in store")
    store_docs_url: Optional[str] = Field(description="URL to app documentation")
    store_preview_before_url: Optional[str] = Field(description="Url of the portal app preview before hover")
    store_preview_after_url: Optional[str] = Field(description="Url of the portal app preview after hover")
    has_frontend: bool = Field(description="App has an App UI Frontend, that can be embedded in an iframe")
    research_only: bool = Field(description="Indicates if app is intended for research use only")
    jobs_count: Optional[int] = Field(
        example=10, default=0, description="Number of app jobs (including finished) in examination"
    )
    jobs_count_finished: Optional[int] = Field(
        example=8, default=0, description="Number of app jobs that have finished in examination"
    )


class AppList(RestrictedBaseModel):
    items: List[App] = Field(description="List of apps (affected by skip/limit pagination)")
    item_count: ItemCount = Field(description="Number of apps (not affected by skip/limit pagination)")


class PostAppResponse(RestrictedBaseModel):
    examination_id: str = Field(example="b10648a7-340d-43fc-a2d9-4d91cc86f33f", description="Examination ID")
    app_id: Id = Field(example="b10648a7-340d-43fc-a2d9-4d91cc86f33f", description="App ID")


class FrontendToken(RestrictedBaseModel):
    access_token: str = Field(
        example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiI3NTYzMDAxOC03ZjhmLTQ3YjgtODZmNC0wMTliODhjNjZhMTEiLCJle"
        "HAiOjE2MjQzNTkwNTZ9.CnT0NYwVzyNl05Jp0z4W-qDqKjolQHZxmT9i7SYyBYG6D-5K7jLxm3l4lBLp30rnjYOiZm0TtvskK1lYDh"
        "gKNyXnEhB_O7f6DQuTd9tn8yv8XnK19pj6g8nubFfBho9lYhComb6a3XX3vqLK5pnaXuhC9tFdzsnLkQPoIi2DZ8E",
        description="AccessToken based on JSON Web Tokens. Allows retrieval of app frontend resources",
    )
