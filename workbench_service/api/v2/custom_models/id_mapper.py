from enum import Enum


class MappingType(Enum):
    CASES = "cases"
    SLIDES = "slides"
