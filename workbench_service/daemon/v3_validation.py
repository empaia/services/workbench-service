import asyncio

from ..py_ead_validation.py_ead_validation.exceptions import JobValidationError
from .case_partitioning import get_case_id
from .singletons import http_client, job_validator, logger, mds_url, mps_url, organization_id, settings


async def validate_jobs():
    try:
        # we want to concurrently execute both input and output validation tasks
        # this logic merely awaits queries and schedules, it does not await the validation
        jobs_for_input_validation, jobs_for_output_validation = await asyncio.gather(
            _fetch_jobs_ready_for_input_validation(), _fetch_jobs_ready_for_output_validation()
        )
        await asyncio.gather(
            _schedule_input_validation_tasks(jobs_for_input_validation),
            _schedule_output_validation_tasks(jobs_for_output_validation),
        )
    except Exception as exc:
        logger.error(exc, exc_info=True)


async def _fetch_jobs_ready_for_input_validation():
    query = {
        "statuses": ["READY", "SCHEDULED", "RUNNING", "COMPLETED", "ERROR"],
        "input_validation_statuses": ["NONE"],
    }
    jobs_object = await http_client.put(f"{mds_url}/v3/jobs/query", json=query)
    return jobs_object["items"]


async def _fetch_jobs_ready_for_output_validation():
    query = {
        "statuses": ["COMPLETED", "ERROR"],
        "output_validation_statuses": ["NONE"],
    }
    jobs_object = await http_client.put(f"{mds_url}/v3/jobs/query", json=query)
    return jobs_object["items"]


async def _schedule_input_validation_tasks(jobs):
    await _schedule_validation_tasks(jobs, "input")


async def _schedule_output_validation_tasks(jobs):
    await _schedule_validation_tasks(jobs, "output")


async def _schedule_validation_tasks(jobs, i_or_o):
    # concurrently begin validation and corresponding status update (see below)
    await asyncio.gather(*[_update_to_running_and_schedule_validation(job, i_or_o) for job in jobs])


async def _update_to_running_and_schedule_validation(job, i_or_o):
    # ensure that input-validation-status update is combined with scheduling
    # (no concurrency between those two things) in order to not schedule the
    # task if any required previous request failed
    job_id = job["id"]

    skip_validation_settings = {
        "input": settings.disable_input_validation,
        "output": settings.disable_output_validation,
    }

    # skip validation if set in settings
    # set validation status to ERROR with corresponding error message
    if skip_validation_settings[i_or_o]:
        await _update_validation_status(job_id, i_or_o, "ERROR", "Validation skipped due to environment settings.")
        return

    update = {"validation_status": "RUNNING"}
    ead = await _get_ead(job["app_id"])
    await http_client.put(f"{mds_url}/v3/jobs/{job_id}/{i_or_o}-validation-status", json=update)
    asyncio.create_task(_run_validation(job, ead, i_or_o))


async def _run_validation(job, ead, i_or_o):
    if i_or_o == "input":
        validation = job_validator.validate_inputs
    else:
        validation = job_validator.validate_outputs
    try:
        case_id = await get_case_id(job["id"])
        await validation(job, ead, case_id=case_id)
        await _update_validation_status(job["id"], i_or_o, "COMPLETED")
    except JobValidationError as exc:
        await _update_validation_status(job["id"], i_or_o, "ERROR", str(exc))


async def _update_validation_status(job_id, i_or_o, validation_status, error_message=None):
    update = {"validation_status": validation_status, "error_message": error_message}
    await http_client.put(f"{mds_url}/v3/jobs/{job_id}/{i_or_o}-validation-status", json=update)


async def _get_ead(app_id):
    headers = {"organization-id": organization_id}
    app = await http_client.get(f"{mps_url}/v1/customer/apps/{app_id}", headers=headers)
    return app["ead"]
