import logging

from pydantic import ValidationError

from ..empaia_sender_auth import AioHttpClient, AuthSettings
from ..py_ead_validation.py_ead_validation.job_validator import JobValidator
from ..settings import Settings
from ..simple_cache import SimpleCache

settings = Settings()
as_url = settings.app_service_url.rstrip("/")
jes_url = settings.job_execution_service_url.rstrip("/")
mds_url = settings.medical_data_service_url.rstrip("/")
mps_url = settings.marketplace_service_url.rstrip("/")
client_id = settings.client_id
client_id_trimmed = client_id[:50]
organization_id = settings.organization_id

logging.basicConfig()
if settings.debug:
    logging.root.setLevel(logging.DEBUG)
else:
    logging.root.setLevel(logging.INFO)
logger = logging.getLogger("WBD")

auth_settings = None
if settings.idp_url:
    try:
        auth_settings = AuthSettings(
            idp_url=settings.idp_url, client_id=settings.client_id, client_secret=settings.client_secret
        )
    except ValidationError as e:
        logger.info(f"Auth not configured: {e}")

http_client = AioHttpClient(
    logger=logger,
    auth_settings=auth_settings,
    chunk_size=settings.connection_chunk_size,
    timeout=settings.http_client_timeout,
    request_timeout=settings.request_timeout,
    connection_limit_per_host=settings.connection_limit_per_host,
)

job_validator = JobValidator(mds_url, http_client)
v3_job_case_cache = SimpleCache()
