import asyncio

import zmq
import zmq.asyncio

from ..daemon_tick import make_daemon_tick
from .singletons import http_client, logger, settings
from .v1_scheduling import schedule_ready_jobs as v1_schedule_ready_jobs
from .v3_preprocessing import setup_preprocessing_jobs as v3_setup_preprocessing_jobs
from .v3_scheduling import schedule_ready_jobs as v3_schedule_ready_jobs
from .v3_validation import validate_jobs as v3_validate_jobs


async def run_async():
    context: zmq.asyncio.Context = zmq.asyncio.Context()
    socket: zmq.Socket = context.socket(zmq.PULL)
    socket.setsockopt(zmq.CONFLATE, 1)  # only last message is relevant
    socket.bind(f"tcp://*:{settings.daemon_zmq_port}")

    while True:
        await socket.recv()
        logger.debug("Tock!")

        await v3_setup_preprocessing_jobs()
        await v3_schedule_ready_jobs()
        await v1_schedule_ready_jobs()
        await v3_validate_jobs()


async def run_trigger():
    daemon_tick = make_daemon_tick(address=f"tcp://localhost:{settings.daemon_zmq_port}", logger=logger)

    while True:
        daemon_tick()
        logger.debug("Sleep %d seconds...", settings.daemon_poll_interval)
        await asyncio.sleep(settings.daemon_poll_interval)


def run():
    loop = asyncio.get_event_loop()
    loop.create_task(http_client.update_token_routine())
    loop.create_task(run_trigger())
    loop.run_until_complete(run_async())
