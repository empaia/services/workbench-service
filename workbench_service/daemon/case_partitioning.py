from .singletons import http_client, mds_url, settings, v3_job_case_cache


async def get_case_id(job_id):
    if not settings.enable_annot_case_data_partitioning:
        return None

    case_id = v3_job_case_cache.get(job_id)
    if case_id is not None:
        return case_id

    raw_examinations = await http_client.put(f"{mds_url}/v3/examinations/query", json={"jobs": [job_id]})
    raw_examination = raw_examinations["items"][0]
    case_id = raw_examination["case_id"]
    v3_job_case_cache.add(job_id, case_id)
    return case_id
