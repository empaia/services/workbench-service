#!/usr/bin/env bash

wbsctl create-access-token-tool-keys && uvicorn workbench_service.app:app $@
